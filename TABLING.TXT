To simplify saving and recalling results of computations between sessions, the following syntactic sugar was created on top of SQLite primitives:


1) Creating a table in the database

@create(DBName,tableName(column1Type,column2Type,...columnNType))
@create(tableName(column1Type,column2Type,...columnNType))

If DBName is not specified, a default DBName (dfpcasedata.db in the current folder) is used instead.

three types of columns are currently supported:

text - an arbitrary sequence of characters (saved / recalled as-is)
clob - prolog term (comples prolog terms are saved as a string and parsed again when they are fetched from the database)
integer / real - numeric type saved and restored as numerical objects


2) Inserting data into previously created table

@insert(DBName,tableName(column1Data,column2Data, ... columnNData)).
@insert(tableName(column1Data,column2Data, ... columnNData)).

If DBName is not specified, a default DBName (dfpcasedata.db in the current folder) is used instead.


3) Requesting (all) data from a table

@(DBName,tableName(Var1,Var2,...VarN))
@(tableName(Var1,Var2,...VarN))

The above predicates etrieve each row from table tableName and unify the row data with Var1,Var2,...VarN.
If DBName is not specified, a default DBName (dfpcasedata.db in the current folder) is used instead.


4) Dropping table from a database

@drop(DBName,tableName)
@drop(tableName)