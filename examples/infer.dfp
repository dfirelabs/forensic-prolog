names(['James', 'John', 'Robert', 'Michael', 'William', 'David', 'Richard', 'Joseph', 'Thomas', 'Charles', 'Christopher', 'Daniel', 'Matthew', 'Anthony', 'Donald', 'Mark', 'Paul', 'Steven', 'Andrew', 'Kenneth', 'George', 'Joshua', 'Kevin', 'Mary', 'Patricia', 'Jennifer', 'Linda', 'Elizabeth', 'Barbara', 'Susan', 'Jessica', 'Sarah', 'Margaret', 'Karen', 'Nancy', 'Lisa', 'Betty', 'Dorothy', 'Sandra', 'Ashley', 'Kimberly', 'Donna', 'Emily', 'Carol', 'Michelle', 'Amanda']).
nouns([book, business, child, company, country, day, fact, family, government, group, hand, home, job, life, man, money, month, mother, night, number, place, point, problem, woman, program, question, room, school, state]).
plural(book,books).
plural(business,businesses).
plural(child,children).
plural(company, companies).
plural(country, countries).
plural(day, days).
plural(fact, facts).
plural(family, families).
plural(government, governments).
plural(group, groups).
plural(hand, hands).
plural(home, homes).
plural(job, jobs).
plural(life, lives).
plural(man, men).
plural(woman, women).
plural(money, monies).
plural(month, months).
plural(mother, mothers).
plural(night, nights).
plural(number, numbers).
plural(place, places).
plural(point, points).
plural(problem, problems).
plural(program, programs).
plural(question, questions).
plural(room, rooms).
plural(school, schools).
plural(state, states).

make_inference(Text,Answer) :- 
    I is floor(random * 4),
    V is random,
    if (V < 0.67, 
    (
        make_valid_inference(Text,I,Answer)
    ), 
    (
        Answer = inv,
        make_invalid_inference(Text,I)
    )).

choose_random(Xn,Yn,Zn, LNouns) :-
        Xn is floor(random * LNouns),
        Yn is floor(random * LNouns),
        Zn is floor(random * LNouns).

all_different(Xn,Yn,Zn) :- 
        Yn \== Xn, Yn \== Zn, Zn \== Xn.

pick_words(X,Y,Z,Xs,Ys,Zs,N) :-
    % pick nouns
    nouns(Nouns),length(Nouns,LNouns),
    repeat,
        choose_random(Xn,Yn,Zn,LNouns),
        all_different(Xn, Yn, Zn),
        !,
    nth0(Xn,Nouns,X), plural(X,Xs), 
    nth0(Yn,Nouns,Y), plural(Y,Ys),
    nth0(Zn,Nouns,Z), plural(Z,Zs),
    % pick a name
    names(Names),length(Names,LNames),
    Nn is floor(random * LNames),
    nth0(Nn,Names,N).


% --- Making valid inferences ---

% Every X is a Y, and all Ys are Zs. Therefore, every X is a Z. (MP)

make_valid_inference(Text,0,mp) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,['Every ',X,' is a ',Y,', and all ',Ys,' are ',Zs,'. Therefore, every ',X,' is a ',Z,'.']).
    
% N is a Y, and all Ys are Zs. Therefore N is a Z. (MP)

make_valid_inference(Text,1,mp) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,[N,' is a ',Y,', and all ',Ys,' are ',Zs,'. Therefore, ',N,' is a ',Z,'.']).

% All Ys are Zs, and no Xs are Zs. Therefore, no Xs are Ys. (MT)

make_valid_inference(Text,2,mt) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,['All ',Ys,' are ',Zs,', and no ',Xs,' are ',Zs,'. Therefore, no ',Xs,' are ',Ys,'.']).

% N is not a Z, and all Ys are Zs. Therefore, N is not a Y. (MT)

make_valid_inference(Text,3,mt) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,[N,' is not a ',Z,', and all ',Ys,' are ',Zs,'. Therefore, ',N,' is not a ',Y,'.']).


% Invalid:

% Every X is a Y, and all Zs are Ys. Therefore, every Z is a X.

make_invalid_inference(Text,0) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,['Every ',X,' is a ',Y,', and all ',Zs,' are ',Ys,'. Therefore, every ',Z,' is a ',X,'.']).
  

% N is a Z, and all Ys are Zs. Therefore, N is a Y.

make_invalid_inference(Text,1) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,[N,' is a ',Z,', and all ',Ys,' are ',Zs,'. Therefore, ',N,' is a ',Y,'.']).

% All Ys are Zs, and no Xs are Ys. Therefore, no Xs are Zs.

make_invalid_inference(Text,2) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,['All ',Ys,' are ',Zs,', and no ',Xs,' are ',Ys,'. Therefore, no ',Xs,' are ',Zs,'.']).

% N is not a X, and every X is a Y. Therefore, N is not a Y.

make_invalid_inference(Text,3) :- 
    pick_words(X,Y,Z,Xs,Ys,Zs,N),
    writes(Text,[N,' is not a ',X,', and every ',X,' is a ',Y,'. Therefore, ',N,' is not a ',Y,'.']).

% Creating Blackboard input file 

make_line(F,T,mt) :-
    writel(F,['MC	',T,'	This is an example of Modus Ponens	incorrect	This is an example of Modus Tollens	correct	This is an invalid infrence	incorrect\n']).

make_line(F,T,mp) :-
    writel(F,['MC	',T,'	This is an example of Modus Ponens	correct	This is an example of Modus Tollens	incorrect	This is an invalid infrence	incorrect\n']).

make_line(F,T,inv) :-
    writel(F,['MC	',T,'	This is an example of Modus Ponens	incorrect	This is an example of Modus Tollens	incorrect	This is an invalid infrence	correct\n']).

make_mcq_file(File,N) :-
    open(File,w,F),
    for(I,1,N),
    make_inference(T,A),
    make_line(F,T,A).
