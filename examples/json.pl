
/*
**  Simple JSON encoder
**
**    PROLOG                                 JSON
**    --------------------------------------------------------------------------
**    123                               <->    123
**    text                              <->   "text"
**    property(value)                   <->   "property" : value
**    array([value1, value2, ...])      <->   [value1, value2, ...]
**    [ prop1(val1), prop2(val2), ...]  <->   { "prop1" : val1, "prop2" : val2, ... }
**
**    For example:
**
**    [color(red),height(5),width(4)]        <->    { "color" : "red", "height" : 5, "width" : 6}
**    picture([name('test.png'),type(png)])  <->    "picture" : { "name" : "test.png", "type" : "png" }  
**    data([items(array([1,2,3])])                   <->    "data" : { "items" : [1,2,3] }
**
**    Usage
**
**    json(Term, JSON_Encoding)
**
**    Usage examples:
**
**     encoding:
**
**       json( picture([name('test.png'),type(png)]) , JSON)
**    
**     decoding:
**
**       json( X, '"data" : { "items" : [1,2,3] }')
**
*/


/* Top-level */

json(Term,JSON) :-
    atom(JSON),                        
    parse(json_grammar(Term),JSON).

json(Term,JSON) :-
    var(JSON),
    term_to_json(Term,JSON).

/* ================= JSON parser grammar ==================== */    

json_number(Num) ::= `(?:\+|-)?\d+(?:(?:e|E)(?:\+|-)?\d+)?`([NumText]), { to_number(NumText,Num) }.

json_atom(Atom) ::= `"((?:[^"\\]|\\.)*)"`([_,Atom]).

json_optional_whitespace ::= `\s+`.

json_list(Term) ::= `\{`,json_term(Head),json_list_tail(Tail), { Term = [ Head | Tail ] }.
json_list_tail([]) ::= `\s*}`.
json_list_tail(List) ::= `\s*,`,json_term(Head),json_list_tail(Tail), { List = [ Head | Tail ] }.

json_array(Term) ::= `\[`,json_term(Head),json_array_tail(Tail), { Term =.. [ array, [Head | Tail] ] }.
json_array_tail([]) ::= `\s*]`.
json_array_tail(Array) ::= `\s*,`,json_term(Head),json_array_tail(Tail), { Array = [ Head | Tail ] }.

json_key_value(Term) ::= json_atom(Key),`\s*:`,json_term(Value), {Term =.. [Key, Value]}.

json_term(X) ::= json_atom(X).
json_term(X) ::= json_number(X).
json_term(X) ::= json_list(X).
json_term(X) ::= json_array(X).
json_term(X) ::= json_key_value(X).
json_term(X) ::= json_optional_whitespace,json_term(X).

json_grammar(Term) ::= json_term(Term),json_end.

json_end ::= end_of_data.
json_end ::= json_optional_whitespace, end_of_data.

/* ================= JSON encoder ==================== */

/* Numbers appear in JSON "as is" */
term_to_json(Term,Json) :-   
    number(Term),
    writes(Json,[Term]).          

/* Non-numbers are put in double-quotes */
term_to_json(Term,Json) :- 
    atom(Term),
    not(number(Term)),
    writes(Json,['"',Term,'"']).   

/* Compound terms of the form array([a1,a2,a3,...]) become [a1,a2,a3,...] */    
term_to_json(array(List),Json) :- 
    is_list(List),
    maplist(json,List,JList),
    writes(Json,[JList]),
    !.

/* Compound terms with a single argument like fun(arg) becomes key-value pair "fun" : arg */    
term_to_json(Term,Json) :- 
    compound(Term),
    arity(Term,1),
    Term =.. [Fun, Arg],                   /* note that fun(arg) =.. [fun, arg]   */
    term_to_json(Fun,JFun),
    term_to_json(Arg,JArg), 
    writes(Json,[JFun,' : ',JArg]).

/* 
** List of terms like [f(x), g(j), k(a,b)] is converted  
** into a sequences of comma-separated JSON encodings surrounded with curly braces
** like { "f" : x, "g" : j, "k" : [ a, b ] }
*/

term_to_json(Term,Json) :-
    is_list(Term),
    Term = [ Element ],                        /* If Term is a list with a single Element */
    term_to_json(Element, JElement),                   /* Produce JSON encoding of (JSONify) the Element. */
    writes(Json, ['{ ',JElement,' }']).        /* The overall encoding is then "{ "+JElement+" }" */

term_to_json(Term,Json) :-
    is_list(Term),
    length(Term,N),
    N > 1,                                      /* If Term is a list with more than one element */
    Term = [Head | Tail],                       /* split it into Head and Tail */
    term_to_json(Head,JHead),                           /* JSONify the Head (the result is JHead) */
    maplist(json,Tail,JTail),                   /* JSONify each element in the Tail and collect the resutling encodings into the list JTail */
    maplist(prepend_with_comma,JTail,JCTail),   /* Prepends each JSON encoding in JTail with a ', ' and collect the resulting strings into list JCTail*/
    writes(JsonTail,JCTail),                    /* Concatenate all elements of JCTail into a single string JsonTail */
    writes(Json,['{ ',JHead,JsonTail,' }']).    /* Produce the overall encoding of Term by concatenating '{' + JHead + JsonTail + '}' */

prepend_with_comma(TextIn, TextOut) :-
   writes(TextOut,[', ',TextIn]).

/*

X = '{ "name":"John","age":30, "cars": [ { "name":"Ford", "models":[ "Fiesta", "Focus", "Mustang" ] },{ "name":"BMW", "models":[ "320", "X3", "X5" ] },{ "name":"Fiat", "models":[ "500", "Panda" ] }]}'



*/