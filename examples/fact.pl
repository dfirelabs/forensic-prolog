fact(N,F) :- N>0, _N is N-1, fact(_N,_F), F is _F * N.
fact(N,F) :- N=0, F=1.
fact_3(N,F) :- compile_3(N,F,_I),element_of(_I,_E).
fact_4(N,F) :- compile_4(N,F,_I),element_of(_I,_E).
fact_5(N,F) :- compile_5(N,F,_I),element_of(_I,_E).
fact_6(N,F) :- compile_6(N,F).
fact_7(N,F) :- compile_7(N,F,_I),nextsol(_I).

nextsol(R) :- get(R,the(S)),(S=yes; S=no, !; nextsol(R)).

:- python('

from decimal import *

def fact(x):
   f = Decimal(1)
   m = Decimal(1)
   while m<=x:
     f = f*m
     m = m+1
   return f

').
fact_p(N,F) :- pyset(n,N),python('f=fact(n)').