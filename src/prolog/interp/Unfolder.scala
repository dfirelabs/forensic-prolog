package prolog.interp

import prolog.terms.SystemObject
import prolog.terms.Term
import prolog.terms.Trail
import prolog.io.{IO,TermParser}

class Unfolder(prog: Prog, val goal: List[Term], atClause: Iterator[List[Term]])
  extends SystemObject {
  def this(prog: Prog) = this(prog, null, Nil.iterator)
  type CLAUSE = List[Term]
  type GOAL = List[Term]

  private val oldtop = prog.trail.size
  def lastClause = !atClause.hasNext

  private final def unfoldWith(cs: CLAUSE, trail: Trail): GOAL = {
    trail.unwind(oldtop)

    goal match {
      case Nil => Nil
      case g0 :: xs =>
      {
        if (cs.head.matches(g0, trail)) {

          // Printing info about trying the next rule in the definition of a predicate
          if (Prog.debuggingMode) 
          {
            if (null != cs.head.source_fname)
            {
              if (Prog.continuing) // check if the user pressed STOP or we reached a breakpoint
              {
                  IO.debug_println("### Next Location: "+cs.head.source_fname+"@"+cs.head.pos);
                  
                  Prog.breakpoints.get((cs.head.source_fname,cs.head.pos.line)) match {
                    case Some(b:Boolean) => if (b) Prog.continuing = false
                    case None => 
                  }
                  if (IO.debug_ready) Prog.continuing = false   // keep running until some data arrives to the debug stream
              }
              if (!Prog.continuing)
              { 
                IO.debug_println("### Next Location: "+cs.head.source_fname+"@"+cs.head.pos);
                //IO.debug_println("\nUnifying rule\n\n"+"   "+cs.head.source_fname+"@"+cs.head.pos+":"+TermParser.clause2string(prog.db.revVars,cs)+ "\n\nwith top subgoal\n")
                IO.debug_println("\nUnifying rule\n\n"+TermParser.clause2string(prog.db.revVars,cs)+ "\n\nwith top subgoal\n")
                val revMap = prog.db.revVars
                val lastTerm = goal.last
                goal.foreach
                {
                  x:Term => //IO.debug_println("   "+x.ref.source_fname+"@"+x.ref.pos+":"+TermParser.term2string(revMap, List(x), if (x == lastTerm) "." else ","))
                            IO.debug_println("   "+TermParser.term2string(revMap, List(x), if (x == lastTerm) "." else ","))
                }
                prog.processDebugCommand()
              }
            }
          }

          prog.copier.clear
          val ds = Term.copyList(cs, prog.copier)
          ds.head.unify(g0, trail)
          ds.tail ++ xs
        } else
          null
      }
    }
    // Nil: no more work
    // null: we have failed
  }

  def nextGoal(): GOAL = {
    var newgoal: GOAL = null
    while (newgoal.eq(null) && atClause.hasNext) {
      val clause: CLAUSE = atClause.next
      newgoal = unfoldWith(clause, prog.trail)
    }
    newgoal
  }

  override def toString = "Step:" + goal + ",last=" + lastClause
}

