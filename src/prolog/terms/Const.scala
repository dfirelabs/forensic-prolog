package prolog.terms
import scala.collection.LinearSeq

class Const(val sym: String) extends Nonvar with LinearSeq[Term] {

  override def name = sym
  def len: Int = 0

  override def bind_to(that: Term, trail: Trail) =
    super.bind_to(that, trail) &&
      sym == that.asInstanceOf[Const].sym
 
  override def head : Term = null;
  override def tail : Const = null;
  override def isEmpty : Boolean = true;
  override def apply(idx: Int) : Term = null ;
  override def length : Int = 0;

  override def equals(o:Any) = o match { case c : Const => c.name == this.name; case _ => false }

  override def toString = sym;
}

object Const {
  final val no = new Const("no")
  final val yes = new Const("yes")
  final val nil = new Const("[]")
  final val cmd = new Const("$cmd")
  final val empty = new Const("")
  final def the(X: Term): Const =
    if (X.eq(null)) Const.no
    else {
      val the = new Fun("the", Array[Term](X))
      the.copy.asInstanceOf[Const]
    }
}

