package prolog.terms

import prolog.interp.Prog

abstract class FunBuiltin(sym: String, val arity: Int) extends Fun(sym) {
  var descriptor : String = null;
  override def toString() = if (descriptor != null) descriptor.toString else super.toString;
  override def exec(p: Prog): Int
}