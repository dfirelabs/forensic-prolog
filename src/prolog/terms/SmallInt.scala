package prolog.terms

final case class SmallInt(nval: Long) extends Num {

  def this(s: String) = this(s.toInt)

  override def name = nval.toString

  override def getValue = BigDecimal(nval)

  def toLong : Long = nval
}