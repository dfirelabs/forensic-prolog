package prolog.terms

import scala.collection.mutable.LinkedHashSet

case class TermByRef(val t:Term) { 
  override def equals(t2:Any) = 
    if ( t2.isInstanceOf[TermByRef]) 
      Term.tcompare(this.t,t2.asInstanceOf[TermByRef].t)==0 
    else 
      false;
}

case class PrologMap(m : Map[TermByRef, Term]) extends Nonvar {

  def this(args : List[Term]) = this (
    PrologMap.empty.m ++ args.map(
      f => {
        val fun = f.asInstanceOf[Fun]
        val key = TermByRef(fun.getArg(0));
        val value = fun.getArg(1)
        (key -> value)
      }
    ).toMap
  )

  override def name() =  toString()

  override def toString() = {
    "map{"+m.map(kv => kv._1.t.toString + " : " + kv._2.toString).mkString(", ")+"}"
  }

  def append(m1:PrologMap) = new PrologMap(m ++ m1.m)

  def insert(a:Term, b: Term): PrologMap = new PrologMap(m + (TermByRef(a.ref) -> b.ref))

  def delete(a:Term): PrologMap = new PrologMap(m - TermByRef(a.ref))

  def get(a: Term) : Option[Term] = m.get(TermByRef(a))

  def keys() = Cons.fromList(m.keys.map(_.t).toList)

  def values() = Cons.fromList(m.values.toList)

  def toList() = Cons.fromList(m.map(kv => new Fun(":",Array(kv._1.t,kv._2))).toList)

  def project(a: Term) = if (a.isInstanceOf[PrologMap]) new PrologMap (PrologMap.empty.m ++ m.filterKeys(a.asInstanceOf[PrologMap].m.keySet)) else PrologMap.empty; 

  /* Return set of keys in this map, which are different (or not present in a) */
  def diff(a : Term) = 
    if (a.isInstanceOf[PrologMap]) 
    {
      val that_m = a.asInstanceOf[PrologMap].m;
      var new_m = scala.collection.mutable.Map[TermByRef,Term]();
      that_m.foreach( kv => {
          if(m.contains(kv._1)) 
          {
             if (Term.tcompare_ignore_vars(this.m(kv._1),kv._2)!=0) new_m += kv;
          }
          else
          {
            new_m += kv;
          }
        }
      )
      new PrologMap(new_m.toMap) 
    } 
    else 
      this; 

  def head() = { val h = m.head; new Fun(":",Array(h._1.t,h._2)) }

  def tail() = new PrologMap(m.tail)

  // def reverse() = new PrologMap(m.map( kv => (new TermByRef(kv._2), kv._1.t) ))

  override def bind_to (that: Term, trail: Trail): Boolean = {
    that.isInstanceOf[PrologMap] && 
    {
      var res : Boolean = true
      val that_m = that.asInstanceOf[PrologMap].m
      bind_maps(this.m,that_m,trail)
    }
  }

  def bind_maps(a: Map[TermByRef,Term], b: Map[TermByRef,Term], trail: Trail) : Boolean = 
    if (a.size == 0) 
      if (b.size == 0) 
        true 
      else 
        false 
    else 
      if (b.size == 0) 
        false
      else {
        val a_head = a.head
        val b_head = b.head
        a_head._1.t.ref.unify(b_head._1.t.ref,trail) && a_head._2.ref.unify(b_head._2.ref,trail) && bind_maps(a.tail,b.tail,trail)
      }

    def unsorted_unify_maps(that : Term, trail: Trail): Boolean =
       that.isInstanceOf[PrologMap] && unsorted_bind_maps(this.m, that.asInstanceOf[PrologMap].m, trail)

    def unsorted_bind_maps(a: Map[TermByRef,Term], b: Map[TermByRef,Term], trail: Trail) : Boolean = 
    if (a.size == 0) 
      if (b.size == 0) 
        true 
      else 
        false 
    else 
    {
      a.keySet == b.keySet &&
      a.keySet.forall(key => { val a_value = a(key); b(key).unify(a_value,trail) })
    }

    override def tcopy(dict: Copier): PrologMap = {

     new PrologMap(m.map( 
        kv => (new TermByRef(kv._1.t.tcopy(dict)) -> kv._2.tcopy(dict))).toMap)

    }

    override def vcollect(dict: LinkedHashSet[Term]) {
      m.foreach ( kv => {
        kv._1.t.vcollect(dict)
        kv._2.vcollect(dict)
      })
    }

}

object PrologMap {
  val empty = new PrologMap(Map[TermByRef,Term]())
}
