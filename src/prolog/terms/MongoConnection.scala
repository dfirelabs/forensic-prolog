package prolog.terms

import org.mongodb.scala._
import prolog.terms.mongo_helpers._

class MongoConnection(db_name: String) extends SystemObject {

  val dbName = db_name
  val database = MongoConnection.client.getDatabase(dbName)


  def createCollection (collectionName : String) {
    database.createCollection(collectionName).results()
  }

  def drop() {
    database.drop().headResult()
  }   

  def showCollections() {
    database.listCollectionNames().printResults()
  }

  override def toString(): String = return "{MongoConnection: " + dbName + "}"

}

object MongoConnection {
  val client: MongoClient = MongoClient()
} 
