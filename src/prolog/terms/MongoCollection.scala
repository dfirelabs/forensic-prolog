package prolog.terms

import prolog.fluents._
import scala.collection.immutable.IndexedSeq
import scala.collection.mutable.Queue

import org.mongodb.scala.bson.collection.immutable.Document
import prolog.terms.mongo_helpers._

class MongoCollection(db: MongoConnection, collectionName: String) extends SystemObject {

  var collection: org.mongodb.scala.MongoCollection[Document] = db.database.getCollection(collectionName) 

  def find(filter: String, projection: String) : MongoQueue = {
    collection.find(Document(filter)).projection(Document(projection)).getBsonQueue().asInstanceOf[MongoQueue]
  }

  def insertOne(document : String) {
//    println("document: " + document)
    collection.insertOne(Document(document)).results()
  }

  def removeOne(document : String) {
    collection.deleteOne(Document(document)).results()
  }
  
  def removeMany(document : String) {
    collection.deleteMany(Document(document)).results()
  }

  def drop() {
    collection.drop().results()
  }

  override def toString(): String = "{MongoCollection: " + collectionName + "}"
}

