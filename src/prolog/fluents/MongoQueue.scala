package prolog.fluents

import prolog.terms._
import org.bson.BsonType
import org.mongodb.scala.bson._
import scala.collection.mutable.Queue
import scala.collection.JavaConversions._

class MongoQueue(q : Queue[collection.immutable.Document]) extends TermSource {
  var iterator = q.iterator
  def getElement() = {
    if (iterator.hasNext) bsonValueToTerm(iterator.next.toBsonDocument)
    else null
  }

  def bsonValueToTerm (bsonValue: BsonValue) : Term = { 
    bsonValue.getBsonType match {
      case BsonType.DOCUMENT  => new PrologMap(PrologMap.empty.m ++ bsonValue.asInstanceOf[BsonDocument].toSeq.map(kv => (TermByRef(new Const(kv._1)) -> bsonValueToTerm(kv._2))))
      case BsonType.DOUBLE    => new Real(bsonValue.asInstanceOf[BsonDouble].getValue)
      case BsonType.INT32     => new SmallInt(bsonValue.asInstanceOf[BsonInt32].getValue)
      case BsonType.STRING    => new Const(bsonValue.asInstanceOf[BsonString].getValue)
      case BsonType.OBJECT_ID => new Const(bsonValue.asInstanceOf[BsonObjectId].getValue.toString)
      case BsonType.ARRAY     => Cons.fromArray(bsonValue.asInstanceOf[BsonArray].map(bsonValueToTerm(_)).toArray)
      case _                  => new Const(bsonValue.toString)
    }
  }
}
