package prolog.forensic

import prolog.io
import prolog.terms._
import prolog.fluents._

import scala.collection.mutable._
import java.sql._


class SQLResult(val functorName : String, val rs: ResultSet) extends TermSource {

  val meta = rs.getMetaData()

  def getElement() = {
    if (rs.isClosed()) 
      null
    else {
      if (rs.next) rowToTerm() else null
    }
  }

  // convert current row from the query result into Prolog term
  def rowToTerm() = {
    val records = new ArrayBuffer[Term]()
    for (i <- 1 to meta.getColumnCount()) {
        meta.getColumnType(i) match {
          case Types.INTEGER => records += new SmallInt(rs.getLong(i))
          case Types.DOUBLE => records +=new Real(rs.getBigDecimal(i))
          case Types.CLOB => records += DataBase.parser.parse(rs.getString(i)).head
          case _ => records += new Const(rs.getString(i))
        }
    }
    new Fun(functorName,records.toArray)
  }

  override def stop() = {
    rs.close
  }

}  
 
