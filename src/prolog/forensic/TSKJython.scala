package prolog.forensic

import prolog.terms._
import prolog.io.IO

import org.python.core._;
import org.python.util.PythonInterpreter;

import scala.collection.mutable.ListBuffer;
import collection.JavaConversions._

class TSKJython() extends SystemObject {

  var interp: PythonInterpreter = null

  def start { 
    try { 
      interp = new PythonInterpreter();
      //interp.exec("import problog; import problog.logic;");

    } catch { 
      case e: PyException => { IO.warnmes(e.getMessage()); throw e; }
    }
  }

  def stop {
     if (interp != null) {
       interp.cleanup();
     }
  }

  def setInteractive(b: Boolean) {
    if (interp != null) {
      // interp.setInteractive(b);
    }
  } 

  def set(v: String, term: Term) = {
    if (interp != null) {
      interp.set(v,prologToPython(term));
      1
    }
    else 0;
  }

  def prologToPython(term: Term) : PyObject = {
        term match {        
           case n : SmallInt => new PyLong(n.toLong);
           case b : Real => new PyFloat(b.getValue.toDouble);
           case c : Cons => interp.eval(cons2str(c));  
           case an : Any => new PyString(an.toString);
        }
  }

  def cons2str(c : Cons) : String =
     Cons.to_string(
      {x => x match 
        {
          case l : Cons => cons2str(l);
          case s : Const => "\\\""+s.asInstanceOf[Const].sym+"\\\"";
          case _ => x.toString 
        }
      },
      c)

  def getValue(v: String) = {  
    if (interp != null) {
      pythonToProlog(interp.get(v));
    }
    else null
  }

  def pythonToProlog(data : Any) : Term =
  {
        data match {
           case in : Integer => new SmallInt(in.toLong)
           case lo : Long => new SmallInt(lo)
           case fl : Float => new Real(BigDecimal(fl))
           case db : Double => new Real(BigDecimal(db))
           case st : String => new Const(st)
           case ll : PyLong  => new Real(ll.getValue())
           case ii : PyInteger  => new SmallInt(ii.getValue())
           case ff : PyFloat => new Real(BigDecimal(ff.getValue()))
           case ss : PyString => new Const(ss.toString())
           case ls : PyList => if ( ls.size() == 0) Const.nil else new Cons(pythonToProlog(ls.get(0)),pythonToProlog(ls.subList(1,ls.size())))
           case hm : PyDictionary => new PrologMap(PrologMap.empty.m ++ hm.map(kv => (TermByRef(pythonToProlog(kv._1)) -> pythonToProlog(kv._2))))
           case o: PyObjectDerived => {
              o.getType.fastGetName() match {
                case "Constant" => pythonToProlog(o.__getattr__("value"))
                case "Term" => {  
                  val nargs = o.__getattr__("_Term__arity").asInstanceOf[PyInteger].getValue();
                  val functor = o.__getattr__("_Term__functor").toString;
                  if (nargs == 0)
                    if (functor == "[]") 
                      Const.nil 
                    else
                      new Const(functor);
                  else {
                    val args = o.__getattr__("_Term__args").asInstanceOf[PyTuple].toArray.map(arg => pythonToProlog(arg));
                    if ((functor == ".") && (nargs == 2))
                      new Cons(args(0),args(1))
                    else
                      new Fun(functor, args)
                  }
                }
                case _ => 
                {
                  IO.warnmes("Reading unsupported Problog object: "+o.getType); new Const(o.toString) 
                }
             }
           }
           case a : Any => {
             IO.warnmes("Reading unsupported jython type: "+a.getClass+"  ("+a.toString+") "); 
             new Const(a.toString) 
           }
        }
  }

  def eval(s : String) = {
    if (interp != null) 
      pythonToProlog(interp.eval(s));
    else 
      Const.empty;
  }

  def exec(s : String) : Boolean = {
    if (interp != null) {
      interp.exec(s);
      true;
    }
    else false;
  }

 def runScript(s : String) = {
    if (interp != null) {
      interp.execfile(s);
    }
  }


  override def toString() =
    "{ Jython Object : " + interp.toString() + " }"
}

object TSKJython extends TSKJython {
   start
}