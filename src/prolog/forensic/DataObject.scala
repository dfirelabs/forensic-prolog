package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import BigInt._
import java.util.UUID
import java.io._
import java.net.URI


abstract class DataObject(val uri: URI) extends SystemObject {
   
  override def toString(): String = {
      return "content{" + uri.toString + "}" 
  }

  override def bind_to(that: Term, trail: Trail): Boolean =
  {
    if (that.isInstanceOf[DataObject] && (that.asInstanceOf[DataObject].uri == uri))
      true
    else
      false
  }

  def getSize : Long;

  def getUniquePath : String = uri.getPath;

  def read(buf: Array[Byte], offset: Long, length: Long) : Long;

}

