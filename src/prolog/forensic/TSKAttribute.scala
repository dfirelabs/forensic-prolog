package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import BigInt._
import java.util.UUID
import java.io._
import org.sqlite._
import org.sleuthkit.datamodel._

class TSKAttribute(attribute: BlackboardAttribute) extends SystemObject {

  val a = attribute
   
  override def unify(that: Term, trail: Trail): Boolean =
  {
    if (that.isInstanceOf[TSKAttribute] && (that.asInstanceOf[TSKAttribute].a.equals(this.a)))
      true
    else
      false
  }

  override def toString() = "{"+a.toString()+"}"
}

