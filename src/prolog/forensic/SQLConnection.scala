package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.mutable._
import java.sql._


class SQLConnection(val connectionString: String) extends SystemObject {

    // Establish database connection

    val dbconn = DriverManager.getConnection(connectionString);

    // Get database metadata and extract names of all tables

    val metadata = dbconn.getMetaData()

    var tables : Map[String,Map[String,Int]] = HashMap();  // member field to keep tables info

    def parseTablesInfo(tablesInfo: ResultSet) {
        if (!tablesInfo.isClosed()) {
            val tableName = tablesInfo.getString(3)
            this.tables(tableName) = HashMap();
            tablesInfo.next                      // move to the next result
            parseTablesInfo(tablesInfo)          // keep parsing
        }
    }

    parseTablesInfo(metadata.getTables(null,null,"%",null));

    // Create a statement for querying the database

    val stmt = dbconn.createStatement()

    def executeQuery(functorPrefix: String, query: String) = new SQLResult(functorPrefix, stmt.executeQuery(query));  // wraps query results in a Prolog fluent

    def execute(sql : String) : Boolean = stmt.execute(sql);

    def getResultSet(functorPrefix: String) = new SQLResult(functorPrefix, stmt.getResultSet);

    // Finally, add information about columns to each table in tables()

    tables.keySet.foreach( 
        tableName => {
            val rs = stmt.executeQuery("select * from "+tableName);
            val meta = rs.getMetaData();
            val columns = new HashMap[String,Int]()
            for (i <- 1 to meta.getColumnCount()) {
                columns(meta.getColumnName(i)) = meta.getColumnType(i)
            }
            tables(tableName)=columns
            rs.close
        }
    )

    def getNamesOfTables() : List[String] = tables.keySet.toList

    def getTableColumns(tableName : String) : Map[String,Int] = {
        val tableData = tables.get(tableName)
        if (tableData == None) 
            null
        else
            tableData.get
    }

    // Get Prolog term describing the table (for defining accessor predicates)
    def getTableTerm(tableName : String, termPrefix : String) = {
        val tableData = tables.get(tableName)
        if (tableData == None) 
            null
        else {
            val columns = tableData.get
            new Fun(termPrefix + tableName,    // functor name 
                columns.keySet.map(
                    columnName => new Const("_"+columnName)
                ).toArray
            )
        }
    }
}  
 
