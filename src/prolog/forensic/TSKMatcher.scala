package prolog.forensic
import prolog.fluents._
import prolog.terms._
import java.util.Scanner
import java.util.regex._
import java.io._
import scala.collection.mutable._

class TSKMatcher(in :  InputStream, p: Pattern) extends TermSource {

  var s : java.util.Scanner = new Scanner(in,"ISO-8859-1")
  var groups = new ListBuffer[Term]()

  def getElement() = {
    if (s == null) null
    else {
      val m = s.findWithinHorizon(p,TSKMatcher.defaultHorizon)
      if (m == null) null
      else {
          val matcher = p.matcher(m)
          matcher.find()
          groups.clear()
          for ( i <- 0 to matcher.groupCount())
          {
             groups += new Const(matcher.group(i))
          }
          Cons.fromList(groups.toList) 
      }
    }
  }

  override def stop() = {s = null }

  override def toString() =
    "{ Scanner : " + p.toString() + " }"
}

object TSKMatcher {
  var defaultHorizon: Integer = 50000000;

  def setDefaultHorizon(h: Integer) {
    defaultHorizon = h;
  }

  def getDefaultHorizon() : Integer = defaultHorizon;

}