package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import BigInt._
import java.util.UUID
import java.io._
import org.sqlite._
import org.sleuthkit.datamodel._

class TSKArtifact(artifact: BlackboardArtifact) extends SystemObject {

  val art = artifact

  override def unify(that: Term, trail: Trail): Boolean =
  {
    if (that.isInstanceOf[TSKArtifact] && (that.asInstanceOf[TSKArtifact].art.equals(this.art)))
      true
    else
      false
  }

  def addAttribute(a: TSKAttribute): Integer =
  {
    this.art.addAttribute(a.a);
    1
  }

  def getAttributes(): Term = Cons.fromList(this.art.getAttributes().asScala.toList.map( e => new TSKAttribute(e)))

  override def toString() = "{"+art.toString()+"}"

}

