package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import BigInt._
import java.util.UUID
import java.io._
import org.sqlite._
import org.sleuthkit.datamodel._
import java.net.URI

class TSKContent(content: Content) extends DataObject(new URI("autopsy","",content.getUniquePath,null)) {

   def this( id : Long ) = this(if(TSKCase.active) TSKCase.getContentById(id) else null)

   def this( uri : URI ) = this(if(TSKCase.active && (uri.getScheme== "autopsy")) TSKCase.getContentByPath(uri.getPath) else null)
   
   val  c = content;
   
   def getId : Long = if (c != null) c.getId() else TSKContent.zero

  override def bind_to(that: Term, trail: Trail): Boolean =
  {
    if (that.isInstanceOf[TSKContent] && (that.asInstanceOf[TSKContent].getId == getId))
      true
    else
      false
  }

  def getSize = c.getSize

  override def getUniquePath = c.getUniquePath

  def read(buf: Array[Byte], offset: Long, length: Long) = c.read(buf,offset,length)

  def newArtifact(artifactTypeID: Integer ) = new TSKArtifact(c.newArtifact(artifactTypeID))

}

object TSKContent {
   val zero: Long = 0
}
