package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskfiledetail() extends FunBuiltin("tskfiledetail", 2) {

  override def exec(p: Prog) = {
    val f:AbstractFile = getArg(0).asInstanceOf[TSKContent].c.asInstanceOf[AbstractFile];

    val id = f.getId()
    val name = f.getName()
    val ext = f.getNameExtension()
    val size = f.getSize()
    val dataSource = f.getDataSource()
    ////val dataSourceObjectId = f.getDataSourceObjectId()
    val ranges = f.getRanges()
    //val offset = f.convertToImageOffset(filesOffset:Long)
    val parentPath = f.getParentPath()
    val typ = f.getType()
    val attrType = f.getAttrType()
    ////val attrId = f.getAttributeId()
    val ctime = f.getCtime()
    val crtime = f.getCrtime()
    val atime = f.getAtime()
    val mtime = f.getMtime()
    val uid = f.getUid()
    val gid = f.getGid()
    val metaAddr = f.getMetaAddr()
    val metaSeq = f.getMetaSeq()
    val modes = f.getModesAsString()
    //val mimeType = f.getMIMEType()
    val isVirtual = f.isVirtual()
    val isFile = f.isFile()
    val isDir = f.isDir()
    val isRoot = f.isRoot()
    // f.setMimeType(String mimeType)
    // val modeSet = f.isModeSet(mode: TskData.TSK_FS_META_MODE_ENUM)
    // f.setMd5Hash(md5Hash:String)
    val hash = f.getMd5Hash()
    // f.setKnown(known:TskData.FileKnown) 
    val knownState = f.getKnown()
    //val nonUniquePath = AbstractFile.createNonuniquePath(path:String)
    // ^^^ drops the name of image and filesystem from the full path.
    val files = f.listFiles() // returns list of children files.
    val hasChildren = f.hasChildren()
    val metaType = f.getMetaType()
    val dirType = f.getDirType()
    //val dirTypeStr = f.getDirTypeAsString()
    //val dirFlag = isDirNameFlagSet(flag:TSK_FS_NAME_FLAG_ENUM)
    val dirFlag = f.getDirFlagAsString()
    val metaFlag = f.getMetaFlagsAsString()
    //val metaFlag = isMetaFlagSet(flag:TSK_META_FLAG_ENUM)
    //val content = f.readLocal(buf:Array[Byte],offset:Long, len:Long)
    //setLocalPath(path:String, isAbsolute:Boolean)
    val localPath = f.getLocalPath()
    val localPathAbs = f.getLocalAbsPath()
    val exts = f.exists() // does the external file exist in the local fs?
    val canrd = f.canRead() // can we read local file?
    //val lfhandle = f.loadLocalFile() //opens filesystem handle to it.


    TSKCase.active match {
      case true => {
        putArg(1, 
              Cons.fromList(List(
                new SmallInt(id),
                new Const(name),
                new Const(ext),
                new SmallInt(size),
                new TSKContent(dataSource),
                //new val ranges = f.getRanges()
                new Const(parentPath),
                new Const(typ.getName),
                new SmallInt(attrType.getValue.asInstanceOf[Long]),
                new Date(ctime),
                new Date(crtime),
                new Date(atime),
                new Date(mtime),
                new SmallInt(uid),
                new SmallInt(gid),
                new SmallInt(metaAddr),
                new SmallInt(metaSeq),
                new Const(modes),
                if (isVirtual) new true_() else new fail_(),
                if (isFile) new true_() else new fail_(),
                if (isRoot) new true_() else new fail_(),
                new Const(hash)
    /* val knownState = f.getKnown()
    //val nonUniquePath = AbstractFile.createNonuniquePath(path:String)
    // ^^^ drops the name of image and filesystem from the full path.
    val files = f.listFiles() // returns list of children files.
    val hasChildren = f.hasChildren()
    val metaType = f.getMetaType()
    val dirType = f.getDirType()
    //val dirTypeStr = f.getDirTypeAsString()
    //val dirFlag = isDirNameFlagSet(flag:TSK_FS_NAME_FLAG_ENUM)
    val dirFlag = f.getDirFlagAsString()
    val metaFlag = f.getMetaFlagsAsString()
    //val metaFlag = isMetaFlagSet(flag:TSK_META_FLAG_ENUM)
    //val content = f.readLocal(buf:Array[Byte],offset:Long, len:Long)
    //setLocalPath(path:String, isAbsolute:Boolean)
    val localPath = f.getLocalPath()
    val localPathAbs = f.getLocalAbsPath()
    val exts = f.exists() // does the external file exist in the local fs?
    val canrd = f.canRead() // can we read local file?
              new Date(f.getMtime())*/
              )), 
              p)
      }
      case _ => { -1 }
    }
  }
}
