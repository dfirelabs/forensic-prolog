package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO
import java.io.File
import java.awt.Desktop

class print extends FunBuiltin("print", 1) {

    override def exec(p: Prog) = {

        val a = getArg(0).ref;

        if (!a.isInstanceOf[Const]) 
            0
        else {
            val fileName = IO.expand_file_name(a.asInstanceOf[Const].name)
            val f = new File(fileName)
            f.createNewFile
            Desktop.getDesktop.print(f)
            1
        }
    }

}
