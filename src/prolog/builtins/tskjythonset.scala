package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic.TSKJython
import collection.JavaConverters._
import org.python.core._

final class tskjythonset() extends FunBuiltin("tskjythonset", 2) {

  override def exec(p: Prog) = {
        val variableName = getArg(0).asInstanceOf[Const].name
        TSKJython.set(variableName,getArg(1))
  }
}
