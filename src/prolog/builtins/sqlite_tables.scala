package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

final class sqlite_tables() extends FunBuiltin("sqlite_tables", 2) {

  override def exec(p: Prog) = {
    val dbconn:SQLConnection = getArg(0).asInstanceOf[SQLConnection];
    putArg(1,Cons.fromList(dbconn.getNamesOfTables().map(new Const(_))),p)
  }
}
