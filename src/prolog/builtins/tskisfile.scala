package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskisfile() extends FunBuiltin("tskisfile", 1) {

  override def exec(p: Prog) = {
    val file:AbstractFile = getArg(0).asInstanceOf[TSKContent].c.asInstanceOf[AbstractFile];
    if (file.isFile()) 1 else 0
  }
}
