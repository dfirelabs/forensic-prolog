package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.collection.mutable.ArrayBuffer
import java.nio.{ ByteBuffer, ByteOrder }

// Converts little-endian bit field to list of values
final class bfbe() extends FunBuiltin("bfbe", 3) {

  final def parse( s : Int, d : ByteBuffer, l : Term ) : (Term,Int) =
  {
     if (l.isInstanceOf[Cons])
     {
       val (tail,start) = parse(s,d,l.asInstanceOf[Cons].getBody)
       val width = l.asInstanceOf[Cons].getHead.asInstanceOf[Num].toLong.toInt;
       val x = ((start % 8)+width)
       d.position(start/8)
       (new Cons(new SmallInt(
           if (x <= 8) ((d.get() >>> (start % 8)) & ((1 << width )-1))
           else if (x <= 16) ((d.getShort() >>> (start % 8)) & ((1 << width )-1))
           else if (x <= 32) ((d.getInt() >>> (start % 8)) & ((1 << width )-1))
           else ((d.getLong() >>> (start % 8)) & ((1L << width )-1))
       ),tail),start+width)     
     }
     else
     {
         (Const.nil,s)
     }
  }

  override def exec(p: Prog) = {
    val data = getArg(0).asInstanceOf[Const].name.toCharArray.reverse.map(_.toByte);
    val d = ByteBuffer.wrap(data)
    var l : Term = getArg(1).asInstanceOf[Cons]
    val (res,len) = parse(0,d,l)
    getArg(2).ref.unify(res,p.trail)
    1
  }
}