package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._
import collection.JavaConversions._
import scala.collection.mutable.ListBuffer

final class tskpythonget() extends FunBuiltin("tskpythonget", 2) {

  override def exec(p: Prog) = {
        val v = getArg(0).asInstanceOf[Const].name
        putArg(1,convert(TSKPython.getValue(v)),p)
  }

  def convert(data : Any) : Term =
  {
      data match {
           case bd : BigDecimal =>new Real(bd)
           case ii : java.lang.Integer  => new SmallInt(ii.toLong)
           case ll : java.lang.Long  => new SmallInt(ll)
           case dd : java.lang.Double => new Real(BigDecimal.valueOf(dd))
           case ff : java.lang.Float => new Real(BigDecimal.valueOf(ff.toDouble))
           case ss : String => new Const(ss)
           case ls : java.util.List[_] => if ( ls.size() == 0) Const.nil else new Cons(convert(ls.get(0)),convert(ls.subList(1,ls.size())))
           case hm : java.util.HashMap[_,_] => {
             var l: ListBuffer[Term] = new ListBuffer[Term]()
             val jmap:java.util.Map[_,_] = hm
             jmap.foreach(kv => l += new Fun("-",Array[Term](convert(kv._1),convert(kv._2)))) 
             Cons.fromList(l.toList)
           }
           case a : Any => { println(a.getClass); new Const(a.toString) }
      }
  }
}
