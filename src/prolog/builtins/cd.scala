package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class cd() extends FunBuiltin("cd", 1) {
  override def exec(p: Prog) = {
    val path:String = getArg(0).asInstanceOf[Const].name
    1
  }
}