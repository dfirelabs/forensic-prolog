package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.Base64

final class mimedecode() extends FunBuiltin("mimedecode", 2) {

  override def exec(p: Prog) = {
    val encoded:String = getArg(0).asInstanceOf[Const].sym;
    putArg(1, new Const(new String(Base64.getMimeDecoder.decode(encoded))), p)
  }
}
