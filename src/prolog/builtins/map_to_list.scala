
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class map_to_list() extends FunBuiltin("map_to_list", 2) {
  override def exec(p: Prog) = {
    val map = getArg(0)
    if (map.isInstanceOf[PrologMap])
    {
      putArg(1,map.asInstanceOf[PrologMap].toList(),p);
    }
    else 0
  }
}