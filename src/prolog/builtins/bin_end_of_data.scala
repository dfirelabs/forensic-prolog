package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.nio.ByteBuffer


final class bin_end_of_data() extends FunBuiltin("bin_end_of_data", 2) {

  override def exec(p: Prog) = {
    val data_pack = getArg(0).ref.asInstanceOf[Cons]
    val data = data_pack.getHead.asInstanceOf[Const].name
    val pos = data_pack.getBody.asInstanceOf[SmallInt].nval.asInstanceOf[Int]
    if (data.length-pos ==0) {
       putArg(1,data_pack,p);     
       1
    }
    else
    {
       0
    }
  }
}
