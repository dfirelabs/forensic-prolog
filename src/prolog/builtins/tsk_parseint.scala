package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import java.lang.NumberFormatException

final class tsk_parseint() extends FunBuiltin("tsk_parseint", 3) {

  override def exec(p: Prog) = {
    try {
       val s = getArg(0).toString
       val radix = getArg(1).toLong.toInt
       putArg(2, new Real(Integer.parseInt(s,radix)), p)
    } catch { 
        case e: NumberFormatException => 0 
    }
  }
}