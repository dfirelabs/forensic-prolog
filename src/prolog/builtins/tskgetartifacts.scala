package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskgetartifacts() extends FunBuiltin("tskgetartifacts", 2) {

  override def exec(p: Prog) = {
    val typeID:Integer = getArg(0).asInstanceOf[Num].getValue.toInt;
    TSKCase.active match {
      case true => {
        putArg(1, 
              TSKCase.getArtifacts(typeID),
              p)
      }
      case _ => { -1 }
    }
  }
}
