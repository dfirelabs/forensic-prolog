package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog
import prolog.Main
import scala.collection.mutable._

final class new_debug_engine() extends FunBuiltin("new_debug_engine", 4) {

  override def exec(p: Prog) = {
    val files = getArg(0)
    val answer = getArg(1)
    val goal = getArg(2)
    val db = Prog.make_db(files, p)
    val q = new Prog(db)
    
    q.debuggingModeEngine = true

    Prog.debuggingMode = true
    Prog.init_debug_with(db, answer, goal, q, Main.parser.vars)
    Prog.debuggingMode = false
    putArg(3, q, p)
  }
}