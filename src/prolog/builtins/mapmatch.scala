
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class mapmatch() extends FunBuiltin("mapmatch", 2) {
  override def exec(p: Prog) = {
    val a = getArg(0)
    val b = getArg(1)
    if (a.isInstanceOf[PrologMap])
    {
      if (a.asInstanceOf[PrologMap].project(b).unsorted_unify_maps(b,p.trail)) 1 else 0;
    }
    else 0
  }
}