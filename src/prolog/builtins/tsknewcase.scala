package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic.TSKCase
import prolog.interp.Prog
import java.nio.file._

final class tsknewcase() extends FunBuiltin("tsknewcase", 2) {

  override def exec(p: Prog) = {
    val x = getArg(0)
    IO.println("% creating new case " + x)
    x match {
      case c: Const => {
        val fname = c.sym
        val ok = prolog.forensic.TSKCase.newCase(fname)
        putArg(1,new Const(Paths.get(fname).toAbsolutePath.toString),p)
        ok
      }
      case _ => 0
    }
  }
}
