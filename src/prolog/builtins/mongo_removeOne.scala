
package prolog.builtins

import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog

final class mongo_removeOne() extends FunBuiltin("mongo_removeOne", 3) {
  override def exec(p: Prog) = {
    val db  = getArg(0).asInstanceOf[MongoConnection]
    val collectionName: String = getArg(1).asInstanceOf[Const].name
    val collection = new MongoCollection(db, collectionName)
    val document  = getArg(2).asInstanceOf[Const].name

    collection.removeOne(document)

    putArg(2, getArg(2), p);
  }
}



