package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._


final class tskjythoneval() extends FunBuiltin("tskjythoneval", 2) {

  override def exec(p: Prog) = {
        val expr = getArg(0).asInstanceOf[Const].name
        putArg(1,TSKJython.eval(expr),p);           
  }
}
