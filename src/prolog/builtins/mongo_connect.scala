
package prolog.builtins

import prolog.terms._
import prolog.interp.Prog

final class mongo_connect() extends FunBuiltin("mongo_connect", 2) {
  override def exec(p: Prog) = {
    val dbName: String = getArg(0).asInstanceOf[Const].name
    putArg(1, new MongoConnection(dbName), p)
  }
}




