
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class mapkeys() extends FunBuiltin("mapkeys", 2) {
  override def exec(p: Prog) = {
    val map = getArg(0)
    if (map.isInstanceOf[PrologMap])
    {
      putArg(1,map.asInstanceOf[PrologMap].keys(),p);
    }
    else 0
  }
}