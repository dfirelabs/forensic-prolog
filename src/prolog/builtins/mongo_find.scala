
package prolog.builtins

import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog

final class mongo_find() extends FunBuiltin("mongo_find", 5) {
  override def exec(p: Prog) = {
    val db  = getArg(0).asInstanceOf[MongoConnection]
    val collectionName: String = getArg(1).asInstanceOf[Const].name
    val collection = new MongoCollection(db, collectionName)
    val filter = getArg(2).asInstanceOf[Const].name
    val projection = getArg(3).asInstanceOf[Const].name

    val result = collection.find(filter, projection).asInstanceOf[MongoQueue]

    putArg(4, result, p);
  }
}



