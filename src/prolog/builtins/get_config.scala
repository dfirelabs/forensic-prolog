package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class get_config() extends FunBuiltin("get_config", 1) {
  override def exec(p: Prog) = {
    putArg(0,IO.configuration,p);
  }
}