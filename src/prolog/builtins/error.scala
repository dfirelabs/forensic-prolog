package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

class error extends FunBuiltin("error", 1) {

    override def exec(p: Prog) = {
        val a = getArg(0);
        println(a.toString);
        0
    }

}