
package prolog.builtins

import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog

final class mongo_dropCollection() extends FunBuiltin("mongo_dropCollection", 2) {
  override def exec(p: Prog) = {
    val db = getArg(0).asInstanceOf[MongoConnection]
    val collectionName: String = getArg(1).asInstanceOf[Const].name
    val collection = new MongoCollection(db, collectionName)

    collection.drop()

    putArg(1, getArg(1), p);
  }
}



