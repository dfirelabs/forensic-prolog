package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class to_binary() extends FunBuiltin("to_binary", 3) {

  override def exec(p: Prog) = {
    val n : Int = getArg(1).asInstanceOf[Num].toLong.toInt
    val s : String = getArg(0).asInstanceOf[Num].toLong.toBinaryString.toUpperCase
    val hex : String = if (n<0) s else ("0000000000000000"+s).takeRight(n)
    if (getArg(2).ref.unify(new Const(hex), p.trail)) 1 else 0
  }
}