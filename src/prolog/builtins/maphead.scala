
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class maphead() extends FunBuiltin("maphead", 2) {
  override def exec(p: Prog) = {
    val a = getArg(0)
    if (a.isInstanceOf[PrologMap])
    {
      putArg(1,a.asInstanceOf[PrologMap].head(),p);
    }
    else 0
  }
}