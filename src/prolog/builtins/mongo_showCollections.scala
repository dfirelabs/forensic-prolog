
package prolog.builtins

import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog

final class mongo_showCollections() extends FunBuiltin("mongo_showCollections", 1) {
  override def exec(p: Prog) = {
    val db = getArg(0).asInstanceOf[MongoConnection]
    
    db.showCollections()
    putArg(0, getArg(0), p);
  }
}


