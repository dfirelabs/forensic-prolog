package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskgetattributes() extends FunBuiltin("tskgetattributes", 2) {

  override def exec(p: Prog) = {
    val artifact:TSKArtifact = getArg(0).asInstanceOf[TSKArtifact];
    TSKCase.active match {
      case true => {
        putArg(1,artifact.getAttributes(),p)
      }
      case _ => { -1 }
    }
  }
}
