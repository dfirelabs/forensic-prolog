package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

final class sqlite_get_result() extends FunBuiltin("sqlite_get_result", 3) {

  override def exec(p: Prog) = {
    val dbconn : SQLConnection = getArg(0).asInstanceOf[SQLConnection];
    val functorName : String = getArg(1).asInstanceOf[Const].name;
    putArg(2,dbconn.getResultSet(functorName),p)
  }
}
