
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class maptail() extends FunBuiltin("maptail", 2) {
  override def exec(p: Prog) = {
    val a = getArg(0)
    if (a.isInstanceOf[PrologMap])
    {
      putArg(1,a.asInstanceOf[PrologMap].tail(),p);
    }
    else 0
  }
}