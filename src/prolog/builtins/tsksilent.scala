package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class tsksilent() extends FunBuiltin("tsksilent", 1) {
  override def exec(p: Prog) = {
    if (getArg(0).isInstanceOf[true_])
    {
       prolog.Main.silent = true
    }
    else
    {
       prolog.Main.silent = false
    }
    1
  }
}