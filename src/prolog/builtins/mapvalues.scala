
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class mapvalues() extends FunBuiltin("mapvalues", 2) {
  override def exec(p: Prog) = {
    val map = getArg(0)
    if (map.isInstanceOf[PrologMap])
    {
      putArg(1,map.asInstanceOf[PrologMap].values(),p);
    }
    else 0
  }
}