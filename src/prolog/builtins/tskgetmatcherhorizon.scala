package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.regex._
import scala.collection.mutable._

final class tskgetmatcherhorizon() extends FunBuiltin("tskgetmatchernorizon", 1) {

  override def exec(p: Prog) = {
    putArg(0,new SmallInt(TSKMatcher.getDefaultHorizon().toLong),p)
  }
}
