
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* append two hashmaps */
final class mapadd() extends FunBuiltin("mapadd", 3) {
  override def exec(p: Prog) = {
    val map1:PrologMap = getArg(0).asInstanceOf[PrologMap]
    val map2:PrologMap = getArg(1).asInstanceOf[PrologMap]
    putArg(2,map1.append(map2),p);
  }
}