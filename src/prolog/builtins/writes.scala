package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import prolog.io._
import util.matching.Regex
import scala.collection.mutable._


final class writes() extends FunBuiltin("writes", 2) {

  override def exec(p: Prog) = {
    val data:List[Term] = Cons.toList(getArg(1).asInstanceOf[Cons])
    val b = new StringBuilder
    for (s <- data)
    {
        b ++= s.toString
    }
    putArg(0,new Const(b.toString),p)
    1
  }
}
