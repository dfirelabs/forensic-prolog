
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class mapinsert() extends FunBuiltin("mapinsert", 4) {
  override def exec(p: Prog) = {
    val map = getArg(0)
    val key = getArg(1)
    val value = getArg(2)
    if (map.isInstanceOf[PrologMap])
    {
      putArg(3,map.asInstanceOf[PrologMap].insert(key,value),p);
    }
    else 0
  }
}