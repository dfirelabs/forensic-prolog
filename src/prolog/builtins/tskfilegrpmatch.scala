package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.regex._
import scala.collection.mutable._

final class tskfilegrpmatch() extends FunBuiltin("tskfilegrpmatch", 4) {

  override def exec(p: Prog) = {
    val input = new ReadContentInputStream(getArg(0).asInstanceOf[TSKContent].c);
    val pattern:Pattern = getArg(2).isInstanceOf[true_] match { 
                            case true => { Pattern.compile(getArg(1).asInstanceOf[Const].name,Pattern.DOTALL) }
                            case _ => { Pattern.compile(getArg(1).asInstanceOf[Const].name,Pattern.DOTALL | Pattern.CASE_INSENSITIVE) }
                        }
    
        putArg(3, 
               new TSKMatcher(input,pattern), 
               p)
  }
}
