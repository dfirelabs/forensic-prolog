package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.fluents._

final class source_to_atom() extends FunBuiltin("source_to_atom", 2) {
  override def exec(p: Prog) = {
    val s = getArg(0).asInstanceOf[TermSource]
    putArg(1, s.toAtom(), p)
  }
}
