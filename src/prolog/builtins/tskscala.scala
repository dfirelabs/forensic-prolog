package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

import scala.reflect.ClassTag
import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.{ILoop, NamedParam}

final class tskscala() extends FunBuiltin("tskscala", 1) {

  override def exec(p: Prog) = {
        
     val urls:List[Any] = java.lang.Thread.currentThread.getContextClassLoader match {
     case cl: java.net.URLClassLoader => cl.getURLs.toList
     case _ => { println("classloader is not a URLClassLoader"); List() }
     }
     val parentclasspath = urls.map( k => k.toString )

     val repl: ILoop = new ILoop {

      private val initialCommands = """
        import prolog._, prolog.terms._, prolog.builtins._, prolog.fluents._, prolog.forensic._, prolog.interp._, prolog.io._
        """
        override def createInterpreter: Unit = {
          super.createInterpreter
          intp.beQuietDuring {  
              intp.interpret(initialCommands)
              intp.bind("arg",getArg(0))
          }
        }
     }
     repl.process(new Settings {
         usejavacp.value = true
         deprecation.value = true
         classpath.value = parentclasspath.mkString(java.io.File.pathSeparator)
         embeddedDefaults(Thread.currentThread().getContextClassLoader())
      })
     1
  } 
  
}
