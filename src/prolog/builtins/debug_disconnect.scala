package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class debug_disconnect() extends FunBuiltin("debug_disconnect", 1) {
  override def exec(p: Prog) = {
    IO.debug_disconnect
    1
  }
}