package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.nio.ByteBuffer


final class pad extends FunBuiltin("pad", 4) {

  override def exec(p: Prog) = {
    val data_pack = getArg(2).ref.asInstanceOf[Cons]
    val data = data_pack.getHead.asInstanceOf[Const].name
    val nblk : Int = getArg(0).ref.asInstanceOf[Num].toLong.asInstanceOf[Int]
    val blksize : Int = getArg(1).ref.asInstanceOf[Num].toLong.asInstanceOf[Int]    
    val pos = data_pack.getBody.asInstanceOf[SmallInt].nval.asInstanceOf[Int]
    if (data.length-pos >= nblk*blksize)
    {   
       putArg(3,new Cons(data_pack.getHead,new SmallInt(pos+nblk*blksize)),p); 
       1
    }
    else
    {
       0
    }
  }
}
