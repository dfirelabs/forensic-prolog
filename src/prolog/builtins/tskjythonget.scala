package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._
import collection.JavaConversions._

final class tskjythonget() extends FunBuiltin("tskjythonget", 2) {

  override def exec(p: Prog) = {
        val v = getArg(0).asInstanceOf[Const].name
        putArg(1,TSKJython.getValue(v),p)
  }

}
