package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class get_class_name() extends FunBuiltin("get_class_name", 2) {
  override def exec(p: Prog) = {
    putArg(1,new Const(getArg(0).getClass.getName),p);
  }
}