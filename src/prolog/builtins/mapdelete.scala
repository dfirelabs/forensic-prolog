
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class mapdelete() extends FunBuiltin("mapdelete", 3) {
  override def exec(p: Prog) = {
    val map = getArg(0)
    val key = getArg(1)
    if (map.isInstanceOf[PrologMap])
    {
      putArg(2,map.asInstanceOf[PrologMap].delete(key),p);
    }
    else 0
  }
}