package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic.TSKCase
import prolog.interp.Prog

final class tskaddimage() extends FunBuiltin("tskaddimage", 1) {

  override def exec(p: Prog) = {
    val x = getArg(0)
    IO.println("% adding image " + x )
    x match {
      case c: Const => {
        val fname = c.sym
        val ok = prolog.forensic.TSKCase.addImage(fname)
        ok
      }
      case _ => 0
    }
  }
}
