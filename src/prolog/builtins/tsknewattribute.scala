package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tsknewattribute() extends FunBuiltin("tsknewattribute", 4) {

  override def exec(p: Prog) = {
    val artifactTypeID:Integer = getArg(0).asInstanceOf[Num].toLong.toInt;
    val moduleName: String = getArg(1).asInstanceOf[Const].name;
    val value: Term = getArg(2)
    TSKCase.active match {
      case true => {
        val a = new TSKAttribute(new BlackboardAttribute(artifactTypeID,moduleName,value.toString))
        if (a != null)
        {
          putArg(3,a,p)
        }
        else
          0
      }
      case _ => { -1 }
    }

  }
}
