package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.Base64

final class mimeencode() extends FunBuiltin("mimeencode", 2) {

  override def exec(p: Prog) = {
    val plain:String = getArg(0).asInstanceOf[Const].sym;
    putArg(1, new Const(Base64.getMimeEncoder.encodeToString(plain.getBytes)), p)
  }
}
