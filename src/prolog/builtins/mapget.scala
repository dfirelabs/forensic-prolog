
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* retrieves value of given key from hashmap, fails if not found */
final class mapget() extends FunBuiltin("mapget", 3) {
  override def exec(p: Prog) = {
    val map:PrologMap = getArg(0).asInstanceOf[PrologMap];
    val value = map.get(getArg(1));
    if (value == None) 
      0
    else {
      putArg(2,value.get.ref,p);
      1
    }
  }
}