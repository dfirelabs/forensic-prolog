package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.io.TermParser
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import collection.mutable._
import prolog.forensic._

/* Returns term with all variable objects replaced by their names */

final class varname() extends FunBuiltin("varname", 2) {

  def convert(t: Term, revMap : LinkedHashMap[Var, String] ) : Term = {
     t match {
       case x : Var => { 
          val o : Term = x.ref
          val name : Option[String] = if (o.isInstanceOf[Var]) revMap.get(o.asInstanceOf[Var]) else Option(o.toString)
          new Const( if(name == None) x.toString else name.get)
       }
       case f:Fun => {
          var a = new ArrayBuffer[Term]()
          f.args.foreach { 
             e : Term => a += convert(e,revMap)
          }
          f match {
            case _: neck => new Const("true")
            case _: cut => new Const("!")
            case _: Cons => new Cons(a.apply(0),a.apply(1))
            case _ => new Fun(f.name,a.toArray)
          }
       }
       case _ => t
     }
  }


  override def exec(p: Prog) = {
    putArg(1,convert(getArg(0),p.db.revVars),p)
    1
  }
}
