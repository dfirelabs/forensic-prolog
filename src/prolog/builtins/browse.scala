package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO
import java.net.URI
import java.net.URLEncoder
import java.awt.Desktop

class browse extends FunBuiltin("browse", 1) {

    override def exec(p: Prog) = {

        val a = getArg(0).ref;

        if (!a.isInstanceOf[Const]) 
            0
        else {
            val address = a.asInstanceOf[Const].name
            val fulladdress = 
                if (address.matches("^[^:]+:.*")) 
                    address
                else
                    if (address.matches("^[\\S]+(\\.[\\S]+)+(/[\\S]+)*")) 
                        "http://"+address
                    else
                        "http://www.google.com/search?q="+URLEncoder.encode(address)
            Desktop.getDesktop.browse(new URI(fulladdress))
            1
        }
    }

}
