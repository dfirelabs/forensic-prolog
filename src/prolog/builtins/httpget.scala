
package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

/* Creates singleton hashmap */
final class httpget() extends FunBuiltin("httpget", 2) {
  override def exec(p: Prog) = {
    val url:String = getArg(0).asInstanceOf[Const].name
    putArg(1,new Const(scala.io.Source.fromURL(url).mkString),p);
  }
}

  