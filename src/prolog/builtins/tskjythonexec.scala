package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._


final class tskjythonexec() extends FunBuiltin("tskjythonexec", 4) {

  override def exec(p: Prog) = {
        val expr = getArg(0).asInstanceOf[Const].name
        //TSKJython.setInteractive(getArg(1).isInstanceOf[true_])
        if (getArg(3).isInstanceOf[true_])
           try
           { 
              TSKJython.exec(expr) 
              putArg(2,new Const(""),p);
           }
           catch { case e : Throwable => putArg(2,new Const(e.toString),p); } 
         else
         {
              TSKJython.exec(expr) 
              putArg(2,new Const(""),p);           
         }
  }
}
