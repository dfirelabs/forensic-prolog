package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class write_config() extends FunBuiltin("write_config", 1) {
  override def exec(p: Prog) = {
      val a: Term = getArg(0);
      IO.configuration = refCopy(a)
      IO.write_configuration()
    1
  }


  // refCopy(t: Term) :  "Copy term while dereferencing variables":
  // -----
  // A modified version of term copying: instead of substituting fresh variables for the original variables in the term, 
  // first dereference the origianl variables. If after dereferencing a variable it is still unbound (i.e. points to itself 
  // or another Var) use that reference in place of the original variable. Otherwise, copy the referenced object and use 
  // that copy in place of the original variable.

  def refCopy(t: Term) : Term = {
    t match {
      case v:Var => { if (v.ref.isInstanceOf[Var]) v.ref else refCopy(v.ref); }
      case f:Fun => { if (f.name == ":-" && f.len == 2) 
                        new Clause(refCopy(f.getArg(0)),refCopy(f.getArg(1)))
                      else if (f.name == "." && f.len == 2) 
                        new Cons(refCopy(f.getArg(0)),refCopy(f.getArg(1)))
                      else if (f.name == "," && f.len == 2) 
                        new Conj(refCopy(f.getArg(0)),refCopy(f.getArg(1)))
                      else if (f.name == ";" && f.len == 2) 
                        new Disj(refCopy(f.getArg(0)),refCopy(f.getArg(1)))
                      else
                        new Fun (f.name,f.args.map(refCopy(_))) }
      case _ => t.copy
    }
  }
}