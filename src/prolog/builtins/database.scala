package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.io.TermParser
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import collection.mutable._
import prolog.forensic._

/* Returns term with all variable objects replaced by their names */

final class database() extends FunBuiltin("database", 1) {


  override def exec(p: Prog) = {
    val l : ListBuffer[Term] = new ListBuffer[Term]()
    p.db.foreach { 
       kc =>
       {
         kc._2.foreach { ll : List[Term] =>
           l += Cons.fromList(ll)
         }
       }
    }
      
    putArg(0,Cons.fromList(l.toList),p)
    1
  }
}
