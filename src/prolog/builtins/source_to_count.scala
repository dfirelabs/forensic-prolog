package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.fluents._

final class source_to_count() extends FunBuiltin("source_to_count", 3) {
  override def exec(p: Prog) = {
    val toCopy = Const.no != getArg(0)
    val s = getArg(1).asInstanceOf[TermSource]
    var c:Long = 0;
    while(s.getElement != null) c += 1;
    putArg(2, new SmallInt(c), p)
  }
}
