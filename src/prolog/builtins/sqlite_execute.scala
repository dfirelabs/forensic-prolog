package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

final class sqlite_execute() extends FunBuiltin("sqlite_execute", 3) {

  override def exec(p: Prog) = {
    val dbconn : SQLConnection = getArg(0).asInstanceOf[SQLConnection];
    val sql : String = getArg(1).asInstanceOf[Const].name;
    putArg(2,if (dbconn.execute(sql)) Const.yes else Const.no,p)
  }
}
