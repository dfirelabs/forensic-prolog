package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._


final class tskjythonrun() extends FunBuiltin("tskjythonrun", 3) {

  override def exec(p: Prog) = {
        val expr = getArg(0).asInstanceOf[Const].name
        if (getArg(2).isInstanceOf[true_])
           try
           { 
              TSKJython.runScript(expr) 
              putArg(1,new Const(""),p);
           }
           catch { case e : Throwable => putArg(1,new Const(e.getMessage()),p); } 
         else
         {
              TSKJython.runScript(expr) 
              putArg(1,new Const(""),p);           
         }
  }
}
