package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskaddattribute() extends FunBuiltin("tskaddattribute", 2) {

  override def exec(p: Prog) = {
    val artifact:TSKArtifact = getArg(0).asInstanceOf[TSKArtifact];
    val attribute:TSKAttribute = getArg(1).asInstanceOf[TSKAttribute];
    TSKCase.active match {
      case true => {
        artifact.addAttribute(attribute)
      }
      case _ => { -1 }
    }
  }
}
