package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class default_config() extends FunBuiltin("default_config", 1) {
  override def exec(p: Prog) = {
    putArg(0,IO.default_configuration,p)
  }
}