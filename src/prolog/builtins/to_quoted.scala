package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog
import prolog.io._

final class to_quoted() extends FunBuiltin("to_quoted", 2) {
  def escape(raw: String): String = {
     val q = raw.replaceAll("\n","\\\\n").replaceAll("\r","\\\\r")
     if(q.matches("^[!=\\@_A-Za-z].*")) { q } else { "'"+q+"'" }
  }

  override def exec(p: Prog) = {
    if (getArg(0).isInstanceOf[Const])
       putArg(1, new Const(escape(getArg(0).asInstanceOf[Const].name)), p)
    else 
       putArg(1, getArg(0), p)
    1
  }
}
