package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

final class sqlite_connect() extends FunBuiltin("sqlite_connect", 2) {

  override def exec(p: Prog) = {
    val dbname:String = getArg(0).asInstanceOf[Const].name;
    putArg(1,new SQLConnection("jdbc:sqlite:"+dbname),p)
  }
}
