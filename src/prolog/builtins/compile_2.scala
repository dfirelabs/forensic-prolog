package prolog.builtins
import prolog.terms._
import prolog.interp.Prog


/*

The following is intended as a possible representation of compiled prolog
predicates that contain only builtins.

compile_2(X,H) :- type_of(X,'integer'),to_hex(X,H).

*/

final class compile_2() extends FunBuiltin("compile_2", 2) {

  // Function arguments
  val v1 = new Var()
  val v2 = new Var()

  // Function 1
  val fn_1 = new prolog.builtins.type_of;
  // Initialisation of arguments of Function 1
  val fn_1_args = new Array[Term](2)
  fn_1_args(0) = v1
  fn_1_args(1) = new Const("integer")
  fn_1.args = fn_1_args

  // Function 2
  val fn_2 = new prolog.builtins.to_hex;
  // Initialisation of arguments of Function 2
  val fn_2_args = new Array[Term](3)
  fn_2_args(0) = v1
  fn_2_args(1) = new SmallInt(4)
  fn_2_args(2) = v2
  fn_2.args = fn_2_args

  override def exec(p: Prog) = {
    // bind variables to argument (unless already bound)
    v1.set_to(getArg(0))//v1.unify(getArg(0),p.trail)
    v2.set_to(getArg(1))//v2.unify(getArg(1),p.trail)
    val fn_1_res = fn_1.exec(p)
    if (fn_1_res>0) 
    {
      fn_2.exec(p)
    } 
    else fn_1_res
  }
}