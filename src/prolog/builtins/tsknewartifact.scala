package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tsknewartifact() extends FunBuiltin("tsknewartifact", 3) {

  override def exec(p: Prog) = {
    val content:TSKContent = getArg(0).asInstanceOf[TSKContent];
    val artifactTypeID:Integer = getArg(1).asInstanceOf[Num].toLong.toInt;
    TSKCase.active match {
      case true => {
        val a = content.newArtifact(artifactTypeID)
        if (a != null)
        {
          putArg(2,a,p)
        }
        else
          0
      }
      case _ => { -1 }
    }

  }
}
