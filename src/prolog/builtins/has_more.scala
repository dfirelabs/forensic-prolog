package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class has_more() extends FunBuiltin("has_more", 1) {
  override def exec(p: Prog) = {
    val s = getArg(0).asInstanceOf[TermSource]
    if (s.hasNext) 1 else 0
  }
}