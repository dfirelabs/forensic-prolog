package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog
import prolog.io._

final class escape_special_chars() extends FunBuiltin("escape_special_chars", 2) {
  override def exec(p: Prog) = {

    var str = getArg(0).asInstanceOf[Const].name
    str = str.replace("\\", "\\\\")        // escape backslash
    str = str.replace("\"", "\\\"")       // escape quotes

    putArg(1, new Const(str), p);

  }
}
