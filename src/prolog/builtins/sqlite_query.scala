package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._

final class sqlite_query() extends FunBuiltin("sqlite_query", 4) {

  override def exec(p: Prog) = {
    val dbconn : SQLConnection = getArg(0).asInstanceOf[SQLConnection];
    val functorName : String = getArg(1).asInstanceOf[Const].name;
    val query : String = getArg(2).asInstanceOf[Const].name;
    putArg(3,dbconn.executeQuery(functorName,query),p)
  }
}
