package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import java.util.regex._

final class to_dotted_ascii() extends FunBuiltin("to_dotted_ascii", 2) {

  override def exec(p: Prog) = {
    val raw = getArg(0).asInstanceOf[Const].name
    val dotted = to_dotted_ascii.regex.matcher(raw).replaceAll(".")
    if (getArg(1).ref.unify(new Const(dotted), p.trail)) 1 else 0
  }
}

object to_dotted_ascii {
    val regex = Pattern.compile("[\\x00-\\x1F,\\x80-\\xFF]")
}