package prolog.builtins
import prolog.terms._
import prolog.forensic.TSKCase
import prolog.interp.Prog

final class tskcase() extends FunBuiltin("tskcase", 3) {

  override def exec(p: Prog) = {
    if (TSKCase.active)
    {   
       putArg(0, 
            new Const(prolog.forensic.TSKCase.fileName), 
            p)

      putArg(1, 
             Cons.fromList(prolog.forensic.TSKCase.images.toList), 
             p)

      putArg(2, 
             Cons.fromList(prolog.forensic.TSKCase.datasources.toList), 
             p)
      1       
    }
    else
    {
       0
    }  
/**  Examples of how to generate different structures

// From Collection --> Prolog list
    putArg(0, 
           Cons.fromArray(Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// From List --> Prolog list
    putArg(0, 
           Cons.fromList(List(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// N-ary functor.
    putArg(0, 
           new Fun("case",Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)    
*/

  }
}
