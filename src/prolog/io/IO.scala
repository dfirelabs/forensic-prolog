package prolog.io
import prolog.terms._
import prolog.fluents.DataBase
import prolog.interp.Prog
import jline.console.history.FileHistory
import jline.console.completer._
import jline.console._
import jline.Terminal
import java.net.{ServerSocket, Socket, InetAddress}
import java.io.{File, FileInputStream, PrintStream, InputStreamReader, InputStream, OutputStream}
import scala.collection.JavaConversions._


object IO {

  def time = System.currentTimeMillis()

  val t0 = time

  val configFile =  System.getProperty("dfp.home") + File.separator + "dfpconfig.pl"

  var configuration: Term = null

  val default_configuration = new Fun("dfpconfig", 
             Array( 
               Cons.fromList(
                 List(
                  new Fun("-",
                    Array(
                      new Const("caseFile"),
                      Const.empty
                    )
                  ),
                  new Fun("-",
                    Array(
                      new Const("persistentPredicates"),
                      Const.nil
                    )
                  )                  
                 )
               )
             ))
    

  def start = {
    read_configuration()
  }

  def stop = {
    //println("Prolog execution halted")
    //println("total time in ms=" + (time - t0))

    /* Writing system configration */
    write_configuration()

    /* Saving command line history */
    write_cmd_history()
    System.exit(0)
  }


  def write_cmd_history()
  {
    if (stdio.getHistory.isInstanceOf[FileHistory])
      stdio.getHistory.asInstanceOf[FileHistory].flush()
  }

  def read_configuration() {
    /* Attempt reading system configuration */
    try {
       configuration = new TermParser().file2clauses(configFile).head.head;
    } catch { 
      case _ => {
          IO.warnmes("Configuration file "+configFile+" not found. Creating default configuration file.");
          configuration = default_configuration
          IO.write_configuration()
      }
    }
  }

  def write_configuration() {
    try{
      val tw = new TermWriter(configFile)
      tw.putElement(new Const(TermParser.term2string(configuration)));
      tw.putElement(new Const("."))
    } catch {
      case _ => IO.warnmes("Cannot write into configuration file " + configFile + " !")
    }
  }

  final def find_file(f0: String): String = {
    if ("stdio" == f0) return f0
    val fs1 = List(f0 + ".pro", f0 + ".pl", f0 + ".dfp", f0)
    val fs2 = fs1.map(x => System.getProperty("dfp.home") + File.separator + "forensic" + File.separator + x)
    val fs3 = fs1.map(x => System.getProperty("dfp.home") + File.separator + "examples" + File.separator + x) 
    val fs4 = fs1.map(x => System.getProperty("dfp.home") + File.separator + "progs" + File.separator + x)
    val fs = (fs1 ++ fs2 ++ fs3 ++ fs4).dropWhile(x => !new File(x).exists())
    if (fs.isEmpty) {
      IO.warnmes("file not found: " + f0)
      null
    } else
      fs.head
  }

  final def expand_file_name(f0: String): String = {
    val f : File = new File(f0)
    val ext = if (f.getName.contains(".")) "" else ".dfp"
    if (f.isAbsolute) 
      f0 + ext
    else 
      System.getProperty("dfp.home") + File.separator + "progs" + File.separator + f0 + ext
  }

  var debug_ss: ServerSocket = null
  var debug_s: Socket = null
  var debug_in: ConsoleReader = new ConsoleReader();
  var debug_out: PrintStream = new PrintStream(System.out);


  var stdio = new ConsoleReader()
  val history_path:String = System.getProperty("dfp.home") + File.separator + ".ppl_hist";
  val history = new FileHistory(new File(history_path))
  stdio.setHistory(history) 
  stdio.setExpandEvents(false)
  
  var dfpCompleter : Completer = null
  updateCompleterStrings(Prog)

  def updateCompleterStrings(p: Prog) {
    stdio.removeCompleter(dfpCompleter)
    dfpCompleter = new PrologCompleter(p) 
    stdio.addCompleter(dfpCompleter)
    stdio.setCompletionHandler(new PrologCandidateListCompletionHandler(p))
  }

  def readLine(prompt: String): String = {
    IO.stdio.readLine(prompt)
  }

  def debug_listen(port_no: Int) {
     debug_disconnect
     debug_ss = new ServerSocket(port_no,0,InetAddress.getLoopbackAddress)
     debug_s = debug_ss.accept()
     debug_out = new PrintStream(debug_s.getOutputStream)
     debug_in = new ConsoleReader(debug_s.getInputStream,debug_s.getOutputStream)
  }

  def debug_disconnect {
      if (null != debug_ss) 
      {
        debug_ss.close()
        debug_ss = null
      }
      if (null != debug_s) 
      {
        debug_s.close()
        debug_s = null
      }
      debug_in = new ConsoleReader();
      debug_out = new PrintStream(System.out);
  }

  def debug_println(s: Any) {
    if (null != debug_out)
    {
       debug_out.println(s.toString)
    }
  }

   def debug_print(s: Any) {
    if (null != debug_out)
    {
       debug_out.print(s.toString)
    }
  }

  def debug_ready() = (debug_in.getInput.available() != 0)
  
  def debug_readLine(prompt: String): String = {
    if (null != debug_in)
    {
       if (null != debug_out) debug_out.print(prompt)
       debug_in.readLine()
    }
    else
       null
  }

  def println(s: Any) {
    stdio.println(s.toString)
    stdio.flush()
  }

  def print(s: Any) {
    stdio.print(s.toString)
  }

  def errmes(mes: String, culprit: Term, p: Prog) = {
    IO.println("*** " + mes + " in => " + culprit)
    p.stop()
    0
  }

  def warnmes(mes: String) = {
    IO.println("*** " + mes)
    0
  }

  def connectConsole(in : InputStream, out : OutputStream, term: Terminal) {
    stdio = new ConsoleReader(in,out,term)
    stdio.setHistory(history) 
    stdio.setExpandEvents(false)
    /*
    IO.println("stdio.getKeys = "+stdio.getKeys)
    IO.println("stdio.getTerminal = "+stdio.getTerminal)
    IO.println("stdio.getTerminal.getOutputEncoding = "+stdio.getTerminal.getOutputEncoding)
    IO.println("stdio.getTerminal.isEchoEnabled = "+stdio.getTerminal.isEchoEnabled)
    IO.println("stdio.getTerminal.isSupported = "+stdio.getTerminal.isSupported)
    IO.println("stdio.getTerminal.isAnsiSupported = "+stdio.getTerminal.isAnsiSupported)
    */
  }

}

