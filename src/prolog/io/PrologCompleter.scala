package prolog.io
import prolog.terms._
import prolog.fluents.DataBase
import prolog.interp.Prog
import jline.console.history.FileHistory
import jline.console.completer._
import jline.Terminal
import java.net.{ServerSocket, Socket, InetAddress}
import java.io.{File, PrintStream, InputStreamReader, InputStream, OutputStream}
import scala.collection.JavaConversions._
import jline.console.completer._
import jline.console._
import jline.internal._

class PrologCompleter(p:Prog) extends Completer {

      val c : Completer = new AggregateCompleter(
                                new StringsCompleter(p.db.keys.map(key => key.f+"/"+key.n).toSet),
                                new StringsCompleter(
                                    TermParser.builtinMap.map(
                                        kv => kv._1.toString + "/" +
                                              (if(kv._2.isInstanceOf[FunBuiltin]) 
                                                kv._2.asInstanceOf[FunBuiltin].arity.toString
                                              else 
                                                "0")
                                    ).toSet
                                )
                            )


      def complete(buffer: String, cursor : Int, candidates:java.util.List[CharSequence]) : Int = {
          
          // Special case to display parameters of predicates
          if ((cursor > 0) && (buffer.charAt(cursor-1)=='('))
          {
              candidates.add(buffer);
              0
          }
          else
          {
              // find beginning of the command under cursor
              var lastCommandStart = 
                math.max(
                    math.max(
                        buffer.substring(0,cursor).lastIndexOf(';'),
                        buffer.substring(0,cursor)lastIndexOf(',')
                    ),  
                    buffer.substring(0,cursor)lastIndexOf(' ')
                )+1
              val lastCommand : String = buffer.substring(lastCommandStart,cursor)
              val prevCommand : String = buffer.substring(0,lastCommandStart)
              val lastCommandCandidates = scala.collection.mutable.Buffer[CharSequence]();
              c.complete(lastCommand,cursor-lastCommandStart,lastCommandCandidates);

              // The following mapping destroys everything *after* cursor
              // This may need to be done more carefully later.
              candidates.addAll(lastCommandCandidates.map(cnd => prevCommand + cnd))
              0 
           }
      }
}

class PrologCandidateListCompletionHandler(p:Prog) extends CandidateListCompletionHandler {
        
        def getCommonPrefix(c: java.util.List[CharSequence]) : String = {
          if (c.length == 1)
            Ansi.stripAnsi(c.get(0).toString)
          else
            (Ansi.stripAnsi(c.head.toString),getCommonPrefix(c.tail))
                  .zipped
                  .takeWhile(Function.tupled(_ == _))
                  .unzip._1.mkString
        }
      
        override def complete(
            reader: ConsoleReader, 
            candidates: java.util.List[CharSequence],
            pos: Int) : Boolean =  {

        val buf: CursorBuffer = reader.getCursorBuffer()

        // if there is only one completion, then fill in the buffer
        if (candidates.size() == 1) {
            var value : String = Ansi.stripAnsi(candidates.get(0).toString());
            val posOfSlash : Int = value.lastIndexOf('/')
            if (posOfSlash > 0) {
                value = value.substring(0,posOfSlash)
            }

            // Check for special case with single completion when cursor is immediately after a (
            if ((buf.cursor > 0) && (value.charAt(buf.cursor-1)=='(')) {
                // to print parameters of the predicate
                // we first get the predicate name (functor)
                var lastCommand : String = buf.upToCursor
                var lastCommandStart = 
                    math.max(
                        math.max(
                            lastCommand.lastIndexOf(';'),
                            lastCommand.lastIndexOf(',')
                        ),  
                        lastCommand.lastIndexOf(' ')
                    )+1
                val functor = 
                    if (lastCommandStart > 0)
                        lastCommand.substring(lastCommandStart,buf.cursor-1)
                    else
                        lastCommand.substring(0,buf.cursor-1)
    
                reader.println
                p.db.foreach(kv => {
                    if (kv._1.f == functor) {
                        kv._2.foreach(clause => 
                            if (clause != null) {
                             reader.print("\n"+TermParser.clause2string(p.db.revVars,clause.take(1)).dropRight(1))
                            })
                    }
                })
                reader.println
                reader.println
                reader.drawLine
            }
            // fail if the only candidate is the same as the current buffer
            if (value.equals(buf.toString())) {
                return false;
            }

            CandidateListCompletionHandler.setBuffer(reader, value, pos)
            return true;
        }
        else if (candidates.size() > 1) {
            var commonPrefix : String = getCommonPrefix(candidates);
            val posOfSlash : Int = commonPrefix.lastIndexOf('/')
            if (posOfSlash > 0) {
                commonPrefix = commonPrefix.substring(0,posOfSlash) 
            }
            CandidateListCompletionHandler.setBuffer(reader, commonPrefix, pos);
        }

        val shortenedCandidates = scala.collection.mutable.Buffer[CharSequence]();
        for(c <- candidates) {
          val s = c.toString
          val lastCommandStart = 
            math.max(
                math.max(
                    s.lastIndexOf(';'),
                    s.lastIndexOf(',')
                ),
                s.lastIndexOf(' ')
            )+1
          shortenedCandidates.add(s.substring(lastCommandStart))
        }

        reader.println;
        CandidateListCompletionHandler.printCandidates(reader, shortenedCandidates)
        reader.println;

        // redraw the current console buffer
        reader.drawLine;
        true
    }
 }
