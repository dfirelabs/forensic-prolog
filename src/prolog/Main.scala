package prolog
import prolog.interp.Prog
import prolog.io.IO
import prolog.io.TermParser
import prolog.terms.Term
import prolog.forensic.TSKCase
import javax.swing.UIManager

object Main extends App {

  var silent:Boolean = false
  var prog: Prog = null
  var parser: TermParser = null 

  init
  go

  def init = {
     prog = new Prog();
     parser = new TermParser();
  }

  def go = {
    //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
    IO.start
    // Start prolog interpreter, but do the prolog initialisation script first  
    toplevel(List("dfp_init") ++ args.toList)
    IO.stop // println("\nProlog execution halted\n")
    prolog.forensic.TSKCase.close()
  }

  def pluginGo = {
    IO.start;
    // Start prolog interpreter, but do the prolog initialisation script first  
    toplevel(List("dfp_init"))
    while(true) toplevel(List[String]())
  }

  def toplevel(topgoals: List[String]) {

      def printvar(x: (String, Term)) = {
        val a = x._1.toString
        //val b: String = TermParser.term2string(prog.db.revVars(), List(x._2), "")
        val b: String = TermParser.term2string(x._2)
        if (a != b && !a.startsWith("_"))
          IO.println(a + " = " + b)
      }
    
      var i = 0;
      //println("topgoals = "+topgoals);
      while (i < topgoals.length) 
      {
        if (topgoals(i) == "-f")
        {
          if (i == topgoals.length-1) 
          {
            IO.warnmes("Script file name is not specified"); 
            IO.stop
          }
          else
          {
            parser.vars.clear()
            val sv = parser.parse("consult('"+topgoals(i+1)+"')")
            if (null != sv) 
            {
              prog.set_query(sv)
              var more = true
              while (more) 
              {
                val answer = prog.getElement()
                if (answer.eq(null)) more = false
              } 
              IO.stop
            }
          }
        }
        else if (topgoals(i) == "-v")
        {
          println("DigitalFIRE Forensic Prolog (C) Pavel Gladyshev, 2016-2018")
          println("based on STYLA Prolog System (C) Paul Tarau, 2011-2012")
          println("Press Ctrl-D to exit (on Windows press Ctrl-Z + Enter) \n")
          IO.stop
        }
        else
        {
          parser.vars.clear()
          val gv = parser.parse(topgoals(i))
          if (null != gv) 
          {
            prog.set_query(gv)
            var more = true
            while (more) 
            {
              val answer = prog.getElement()
              if (answer.eq(null)) more = false
              else
                parser.vars.foreach(printvar)
            }
          } 
        }
        i=i+1;
      } 

    var goalWithVars = (parser.parse("true. "), parser.vars)

    while (!goalWithVars.eq(null)) {    
      IO.updateCompleterStrings(prog)
      goalWithVars = parser.readGoal()
      IO.write_cmd_history()
      if (!goalWithVars.eq(null)) {
        val goal = goalWithVars._1
        val vars = goalWithVars._2

        prog.set_query(goal)

        // TO DO
        // begin SQL transaction
        // create table with goal as a template

        var more = true
        while (more) {
          val answer = prog.getElement()

          if (answer.eq(null)) more = false
          else {

            // insert another record to the created table

            if (!silent) 
            {
               if (vars.isEmpty) IO.println("yes.")
               else {
                 vars.foreach(printvar)
                 IO.println(";")
               }
            }
          }
        }

        // committ

        if (!silent)
        {
           IO.println("no (more) answers\n")
        }
      }
    }
  }
}
