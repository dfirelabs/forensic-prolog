if not exist bin mkdir bin
if not exist tmp mkdir tmp
if not exist progs mkdir progs
scalac -classpath "./lib/jar/mongo-scala-driver_2.12-2.1.0-alldep.jar;./lib/jar/jython-standalone-2.7.0.jar;./lib/jar/jep.jar;./lib/jar/akka-actor_2.12-2.5.12.jar;./lib/jar/config-1.3.3.jar;./lib/jar/Tsk_DataModel.jar;./lib/jar/sqlite-jdbc-3.8.11.jar;./lib/jar/c3p0-0.9.5.2.jar;./lib/jar/mchange-commons-java-0.2.11.jar" -optimize -explaintypes -deprecation -unchecked -nowarn -d bin  src/prolog/*.scala src/prolog/acts/*.scala src/prolog/builtins/*.scala src/prolog/fluents/*.scala src/prolog/forensic/*.scala src/prolog/interp/*.scala src/prolog/io/*.scala src/prolog/terms/*.scala src/prolog/tests/*.scala
