
jpeg(X) :- content(X),
          content_data(X,0,1024,C),
          matches(C,'JFIF').

print_all_jpegs :- jpeg(X),
                  content_path(X,P),
                  println(P).

print_susp_jpegs :- jpeg(X),
                   content_path(X,P),
                   not(matches(P,'.*.jpg$',i)),
                   println(P).

