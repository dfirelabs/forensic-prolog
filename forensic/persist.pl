
test(X,Y) :- for(X,1,10), Y is X*X.

% Create table and save all solutions of test(X,Y) into it.
saveTest :-
   @create(test(integer,integer)),
   test(X,Y),
   @insert(test(X,Y)).

% Display all values from table test
showTest :-
   @(test(X,Y)),
   writel(['X = ',X,' Y = ',Y,'\n']).
