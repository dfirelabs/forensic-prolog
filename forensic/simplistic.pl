hexdump ::= end_of_data,{!}. % If at the end of data, do nothing.
hexdump ::= h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,{ write('\n') },
            hexdump.

h ::= end_of_data,{!}.    % If at the end of data, do nothing.
h ::= u8(X),{ print(X) }. % Otherwise get next 8-bit value and print it.

print(X):-to_hex(X,2,Y),write(X),write(' ').