% Some examples of using date and time information

% Displaying path and last modified time of all files modified in the 21st century.
files_21a :-
   findall([P,M],
      (
          content(F),
          content_mtime(F,M),
          M >= \2001-01-01T00:00:00Z\,
          content_path(F,P)
      ),L),
    table('Files last modified in 21st Century',['Path','M-Time'],L).
 
% Same as above, but displaying only the Path
files_21b :-
   findall([P],
      (
          content(F),
          content_mtime(F,M),
          M >= \2001-01-01T00:00:00Z\,
          content_path(F,P)
      ),L),
    table('Files last modified in 21st Century',['Path'],L).

% Same as above, but using mtime() function in expression rather than mtimeof/2 predicate
files_21c :-
   findall([P],
      (
          content(F),
          mtime(F) >= \2001-01-01T00:00:00Z\,
          content_path(F,P)
      ),L),
    table('Files last modified in 21st Century',['Path'],L).
 