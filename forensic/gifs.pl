
gif(X) :- content(X),
          matches(X,'^GIF').
          
/* Here is an alternative version. 
*  It reads first three bytes from 
*  file content and matches them 
*  against '^GIF' regexp: 

gif(X) :- content(X),
          content_data(X,0,3,C),
          matches(C,'^GIF').
*/

print_all_gifs :- gif(X),
                  content_path(X,P),
                  println(P).

print_susp_gifs :- gif(X),
                   content_path(X,P),
                   not(matches(P,'.*.gif$',i)),
                   println(P).
