:- use(problog).

model('
   t(_)::coin1(0);
   t(_)::coin1(1).

   t(_)::coin2(0);
   t(_)::coin2(1).

   throw(Z) :- coin1(X),coin2(Y),is(Z,X+Y).
').

evidence([
    [(throw(0),true)],
    [(throw(2),true)],
    [(throw(2),true)],
    [(throw(1),true)]
]).
  
% ?- model(M),evidence(E),lfi(M,E,N), writes(NN,[N,' evidence(throw(1)). query(coin1(_)). query(coin2(_)).']),marg(NN,R),println(NN),println(R).
