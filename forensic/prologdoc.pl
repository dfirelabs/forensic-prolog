% Simple prolog documentation generator 
%
% Documentation is included into the text of the program using %% prefix at the beginning of line:
%
%% Ordinary lines are included into HTML output verbatim.
%% Predicates are specified using the following syntax:
%%
%% predicate example(+X,-Y)
%% parameter +X example input data
%% parameter -Y example output data
%%
%% The subsequent lines can be used to give the description of the predicate.
%% Comments could contain HTML tags, but this is not recommended, given that 
%% the output format could differ.
%% 
%% Two predicates are provided:
%% 
%% predicate parsefile(FileName,Data)
%% parameter +FileName is the name of the prolog program to be processed
%% parameter -Data is the returned parsed list of comment lines
%%
%% predicate writehtml(HtmlFileName,Data)
%% parameter +HtmlFileName is the name of HTML file to create
%% parameter +Data is the parsed result returned by parsefile/2

% --- 1. Parser: extracts and parses comment lines beginning with %% 

readfile(FileName,Result) :-
    open(FileName,cr,F),
    findall(E,element_of(F,E),L),
    atom_chars(Result,L).

parsefile(FileName,Result) :-
    readfile(FileName,Data),
    parse(file(Result),Data).

file([]) ::= end_of_data.
file([S]) ::= doc_line(S),end_of_data.
file([]) ::= normal_line,end_of_data.
file([S|SS]) ::= doc_line(S),`\n`,file(SS).
file(SS) ::= normal_line,`\n`,file(SS).

doc_line(Data) ::= `%%([^\n]*)`([_,S]),{parse(doc_string(Data),S), !}.
normal_line ::= ``.
normal_line ::= `[^%][^\n]*`.

doc_string(predicate(Name)) ::= `\s*predicate\s+([^\s]+)[^\n]*`([_,Name]),{!}.
doc_string(parameter(Name,Text)) ::= `\s*parameter\s+([^\s]+)\s+([^\n]*)`([_,Name,Text]),{!}.
doc_string(text(Text)) ::= `[^\n]*`([Text]).

% --- 2. Generator: oputputs HTML based on parsed end_of_data

writehtml(FileName,Title,Data) :-
    open(FileName,w,F),
    writeprologue(F,Title),
    writedata(F,Data),
    writeepilogue(F).

writeprologue(F,Title) :-
    writel(F,[
'<html>
<head>
<title>',
Title,
'</title>
</head>
<body>'
]).

writeepilogue(F) :-
    writel(F,[
'</body>
</html>
']).

writedata(F,Data) :-
   dataparagraphs(Data,Paragraphs),
   writeparagraphs(F,Paragraphs).

% First we need to pore-process data for HTML output:
% 1) combine non-empty consecutive strings into single paragraph-long strings
% 2) merge predicate(N), followed by parameter(P,T),parameter(P,T),... into predicate(N,[parameter(P,T),parameter(P,T),...])
% we use DCG grammars for that

dataparagraphs(Data,Paragraphs) :-
    phrase(html_preproc(Paragraphs),Data).

html_preproc([]) --> [].
html_preproc([text(P)|PP]) --> html_paragraph(P0),{ writes(P,P0) },html_preproc(PP). 
html_preproc([predicate(P,PL)|PP]) --> html_predicate(P,PL),html_preproc(PP). 

% Paragraph is a sequence of non-empty lines text(X), where X is \== '',
% followed by either a text(''), a non-text functor, or the end of data.

html_paragraph([L]) --> [text(L)], [text('')], {!}.
html_paragraph([L]),[Next] --> [text(L)], { L \== ''}, [Next], { ( (Next = predicate(_), !); (Next = parameter(_), !) ) }.
html_paragraph([L|LL]) --> [text(L)], { L \== ''}, html_paragraph(LL), {!}.
html_paragraph(LL) --> [text('')], html_paragraph(LL), {!}.
html_paragraph([L]) --> [text(L)], { L \== ''}, {!}.

html_predicate(P,PL) --> [predicate(P)],html_parameters(PL),{!}.
html_predicate(P,[]) --> [predicate(P)],{!}.

html_parameters([]), [Next] --> [Next],{ not(Next = parameter(X,D)) }, {!}.
html_parameters([parameter(X,D)|PL]) --> [parameter(X,D)],html_parameters(PL), {!}.
html_parameters([]) --> {!}.


% Writing data: do nothing if there is no data
writeparagraphs(F,[]).  

writeparagraphs(F,[text(T)|Rest]) :- 
    writel(F,['<P>',T,'</P>\n']),
    writeparagraphs(F,Rest).

writeparagraphs(F,[predicate(P,Params)|Rest]) :- 
    writel(F,['  <P>Predicate <em>',P,'</em><ul>\n']),
    writepredicateparams(F,Params),
    writel(F,['  </ul></P>\n']),
    writeparagraphs(F,Rest).

writepredicateparams(F,[]).
writepredicateparams(F,[parameter(N,Descr) | Rest]) :-
    writel(F,['     <li><em>',N,'</em> ',Descr,'</li>\n']),
    writepredicateparams(F,Rest).
    
    