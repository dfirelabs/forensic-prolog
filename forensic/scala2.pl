% This file demonstrates how to embed Scala code into ordinary prolog code
scalaHello :- 
    X = 'Robert', 
    $ 
      println("Hello, " + X + "!"); 
      println("The lenght of name '"+X+"' is " + X.toString.size + " characters")
    $.

scalaArithmetics :-
    Y = [1,2,3],
    writel(['Printing from Prolog: Y = ',Y,'\n']),
    $
      println("Printing from Scala: Y.head + 5 = " + (Y.head + 5));
      val z = Y.map( y=> y+1);
      println("Printing from Scala: z = Y.map( y=>y+1 ) = " + z);
      unify(Z,z)
    $,
    writel(['Printing from Prolog: Z = ',Z,'\n']).

scalaHello2(X) :-
    $ 
      unify(Y," again!\n")
    $,
    writel([X,Y]).

:- scalaHello.
:- scalaArithmetics.
:- scalaHello2('Hello').
