:- use_module('problog2.py').
 
0.1::a(1);
0.1::a(2);
0.1::a(3);
0.1::a(4);
0.1::a(5);
0.1::a(6);
0.1::a(7);
0.1::a(8);
0.1::a(9);
0.1::a(10) <- true.

% Suppose that B also could have been any number from 1 to 10 with equal probability.

0.1::b(1);
0.1::b(2);
0.1::b(3);
0.1::b(4);
0.1::b(5);
0.1::b(6);
0.1::b(7);
0.1::b(8);
0.1::b(9);
0.1::b(10) <- true.

model(A,B,S) :- a(A),b(B),sum(A,B,S).

output(X) :- model(A,B,X).

evidence(output(5),true).

query(model(_,_,_)).