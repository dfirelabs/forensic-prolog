time_of(crtime,T,X) :- content_crtime(X,T).
time_of(mtime,T,X) :- content_mtime(X,T).
time_of(atime,T,X) :- content_atime(X,T).

signature_element_match(SE,T,DT,Path,Timestamp) :- 
   SE = [PN,FN,Type],
   content(File,PN,FN), 
   time_of(Type,Timestamp,File), 
   abs(Timestamp - T) < DT,
   content_path(File,Path).

% Recursive definition of signature matching.
% 
%  Signature is the list of timestamp descriptors of the form:
%       [PathPattern,FilenamePattern,TimestampType]
%     for example 
%       ['%','%.mbx',crtime] refers to creation time of any file whose extension is .mbx 
%
% event() predicate tries to find all matches for the given signature in the currently opened case 
% at the given time T with spread of DT seconds. All matching timestamps are collected in the list L

event(Signature,T,DT,L) :-
   event_aux(Signature,T,DT,[],L).

% event_aux recurses over the elements of the signature and adds
% any matching timestamps to the list L.

% 1. The base case: if the signature is empty (we have finished processing the last 
% file description in it), succeed if we collected some matching files in L0. They are
% make them the final output (L)
event_aux([],T,DT,L0,L) :- L0 \== [], L=L0.

% 2. Interim step: take the first element in the signature and try to find any matches to it
%
% If some matches are found (FS \== []), append them to L0 producing L1 and repeat the process for the rest of signature
% using L1 instead of L0 (it serves as accumulator of found matches).
%
% If no matches are found (FS == []), then simply proceed with finding matches for the rest of the 
% signature, passing L0 as is.

event_aux([Head|Rest],T,DT,L0,L) :-
    findall([Path,Timestamp],signature_element_match(Head,T,DT,Path,Timestamp),FS),
    (
        FS \== [],
        append(FS,L0,L1),
        event_aux(Rest,T,DT,L1,L)
    ;
        FS == [],
        event_aux(Rest,T,DT,L0,L)
    ).

times(LT) :-
   findall(TS,
   (
       content(X), 
       time_of(Type,TS,X)
   ),
   L),
   sort(L,LT).

%  Using signature matcher:
% 
%  1. Collect the list of all distinct times in the case
%  2. Iterate over these times
%  3. Try to find signature match for each of these times.
%
%  ?-  times(LT),member(T,LT),event([['%','%.mbx',crtime]],T,1,L)

   
