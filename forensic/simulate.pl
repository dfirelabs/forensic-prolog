%  Comparison of bit-error influence on comparison of conjunctions using majority and product.


masks(
    [0b00000001,
     0b00000010,
     0b00000100,
     0b00001000,
     0b00010000,
     0b00100000,
     0b01000000,
     0b10000000]
).

random_errors_aux(Din,0,[],Din).
random_errors_aux(Din,N,T,Dout) :- 
     N \== 0,
     N0 is N-1, 
     random_errors_aux(Din,N0,T0,Dout0), 
     masks(Ms),
     length(Ms,L),
     repeat, 
        random_max_int(L,_X),
        if(not(member(_X,T0)), 
        (
          nth0(_X,Ms,Mask),
          Dout is xor(Dout0,Mask),
          T = [_X | T0],
          !
        ),
        fail
        ).
    
random_errors(Din,N,Dout) :- random_errors_aux(Din,N,_,Dout).

simulate(0,X,Y,0,0,0,0).
simulate(N,X,Y,SumCorr,SumErr,ProdCorr,ProdErr) :- 
  N \== 0,
  N0 is N-1,
  simulate(N0,X,Y,SumCorr0,SumErr0,ProdCorr0,ProdErr0),
  random_max_int(8,NXerr),
  random_max_int(8,NYerr),
  random_errors(X,NXerr,XE),
  random_errors(Y,NYerr,YE),
  Xbc is bitcount(XE),
  Ybc is bitcount(YE),
  if(((Xbc == 8),(Ybc \== 8)),
  (
    ProdCorr is ProdCorr0 + 1, 
    ProdErr is ProdErr0
  ), 
  (
    ProdCorr is ProdCorr0,
    ProdErr is ProdErr0 + 1 
  )),
  if((Xbc > Ybc), 
  (
    SumCorr is SumCorr0 + 1, 
    SumErr is SumErr0
  ), 
  (
    SumCorr is SumCorr0,
    SumErr is SumErr0 + 1

  )). 