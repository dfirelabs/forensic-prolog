:-  use(problog).

:-  problog('forensic/fsm-prob2.pl',Res), % Process ProbLog model
    map_to_list(Res,R),                  % Convert returned map into list
    member(M:P,R),                       % Iterate through possible solutions X and their probabilities P
    P =\= 0.0,                           % For each solution that has a non-zero probability
    writel([M,'  -   ',P,'\n']).         % print it out