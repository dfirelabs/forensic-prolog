from problog.extern import problog_export, problog_export_nondet

@problog_export('+int', '+int', '-int')
def sum(a, b):
    """Computes the sum of two numbers."""
    return a + b
