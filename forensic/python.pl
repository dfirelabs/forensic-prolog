% WARNING: Python integration is a work in progress
:- python('

import math

def root(x):
   return math.sqrt(x)

').

root(X,Y) :- 
  pyset(x,X),
  python('y = root(x)'),
  pyget(y,Y).


 
    