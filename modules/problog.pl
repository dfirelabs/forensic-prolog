module(problog,[problog,lfi,marg,show_result]).

% This module encapsulates problog functionality, which is accessible via jython()


:- jython('

from problog import get_evaluatable
from problog.program import PrologFile
from problog.program import PrologString
from problog.learning import lfi
from problog.logic import Term

').

problog_examples(Examples, ExamplesString) :-     
  #
    import prolog.forensic._

    def evidence2str(evidence : Cons) : String =
          Cons.to_string(
          {x => x match 
            {
              case l : Cons => evidence2str(l);
              case s : Fun => 
              "(Term.from_string(\""+s.getArg(0).toString+".\"),"+
              (
                s.getArg(1).asInstanceOf[Const].name match 
                {
                  case "true" => "True";
                  case "false" => "False";
                }
              )+")";
              case _ => x.toString 
            }
          },
          evidence)

    unify(ExamplesString,evidence2str(Examples.asInstanceOf[Cons]))
  #
  .

problog_eval(Expression,Result) :-
  # 

    import prolog.forensic._
    import prolog.io.IO._

    def retry(retries : Integer, expr: String) : Term =
    {
      try {
        TSKJython.eval(expr);
      } catch { 
        case e: Throwable => { 
          if (retries <= 0) {
            throw e;
          }
          else retry(retries-1,expr)
        }
      }
    }
    unify(Result,retry(5,Expression.toString))
  #
  .


% Load ProbLog model specified in the string Model and calculate any queries embedded in it.
marg(Model,Result) :-
  writes(Expression,['get_evaluatable().create_from(PrologString("',Model,'")).evaluate()']),
  problog_eval(Expression,Result).

% Load ProbLog model specified in the file ModelFile and calculate any queries embedded in it.
problog(ModelFile,Result) :-
  writes(Expression,['get_evaluatable().create_from(PrologFile("',ModelFile,'")).evaluate()']),
  problog_eval(Expression,Result).

% Display problog probability distribution in a table, but do not show any rows with 0 probability.
show_result(Res) :-
  map_to_list(Res,Result),
  findall([R,P],(member(R:P,Result),P \== 0.0),L),
  table('',['Answer','Probability'],L).

% Learning from examples (LFI) 
lfi(Model,Examples,NewModel) :-
  problog_examples(Examples,ExamplesString),
  writes(Expression,['lfi.run_lfi(PrologString("',Model,'"),',ExamplesString,')[4].get_model()']),
  jython(Expression,NewModel).
