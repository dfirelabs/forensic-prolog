% This is a demonstration of module mechanics as implemented by use/1 :

module(loops,[test]).  % Defines module name prefix (loops__) and the list of predicates to be defined as-is

test(hello).
test(world).

rule(X) :- 
    test(X),
    writel([X]).

rep :- for(I,1,10000),println(I).